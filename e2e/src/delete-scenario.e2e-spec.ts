import { ScenariosPage } from './Pages/scenarios.po';
import { browser, by, ExpectedConditions } from 'protractor';

/**
 * TESTING FOLLOWING USE CASE:
 * UC4: Delete scenario
 * For detailed information on use cases please see documentation
 */
describe('delete scenario', () => {
  let scenariosPage: ScenariosPage;

  beforeEach(() => {
    scenariosPage = new ScenariosPage();
    scenariosPage.navigateTo();
  });


  it('"Delete scenario" button should delete scenario', () => {
    const scenarioToBeDeleted = scenariosPage.getScenarioByName('Zrak 1');

    // Delete scenario 'Zrak 1'
    scenarioToBeDeleted.element(by.css('.delete')).click();

    // Confirm deleting dialog
    scenariosPage.confirmDialog();

    // Wait for API refresh
    browser.wait(ExpectedConditions.invisibilityOf(scenarioToBeDeleted), 10000);

    expect(scenariosPage.getScenarioByName('Zrak 1').isPresent()).toBeFalsy('scenario should be deleted');
  });

});
