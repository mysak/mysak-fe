import { browser, WebElement } from 'protractor';

export const matchUrl = (url: string) => {
  const baseUrl = browser.baseUrl.endsWith('/') ? browser.baseUrl : browser.baseUrl + '/';
  const match = new RegExp('^' + baseUrl + url + '$');
  expect(browser.getCurrentUrl()).toMatch(match);
};

export const dragAndDrop = (from: WebElement, to: WebElement) => {
  browser.actions()
    .mouseMove(from).mouseDown()
    .mouseMove(from, {x: -10, y: 0})
    .mouseMove(to)
    .mouseUp().perform();
  // wait until queue position changes
  browser.sleep(500);
};
