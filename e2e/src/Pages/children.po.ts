import { browser, by, element } from 'protractor';
import { ChildrenList } from './children.list';

export class ChildrenPage {
  childrenList: ChildrenList;
  constructor() { this.childrenList = new ChildrenList(); }

  getChildByName = (name) => this.childrenList.getChildByName(name);

  navigateTo() { return browser.get('/eduapps/1/children'); }

  getSelectedEduappTitle() { return element(by.css('app-toolbar .mat-select')).getText(); }

  getChildrenLastNames = (group?: string) => this.childrenList.getChildrenLastNames(group);
}
