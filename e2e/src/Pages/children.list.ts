import { by, element } from 'protractor';

export class ChildrenList {

  getChildByName(name: string) { return element(by.cssContainingText('tr', name)); }

  getChildrenLastNames(group: string): Promise<string[]> {
    const rows = element.all(group ? by.cssContainingText('.mat-row', group) : by.css('.mat-row'));
    return rows.map(r => r.element(by.css('.mat-column-lastName')).getText()) as Promise<string[]>;
  }
}
