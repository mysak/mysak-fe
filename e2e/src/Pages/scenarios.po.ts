import { browser, by, element } from 'protractor';
import { ScenariosList } from './scenarios.list';

export class ScenariosPage {
  scenariosList: ScenariosList;
  constructor() { this.scenariosList = new ScenariosList(); }

  getScenarioByName = (name) => this.scenariosList.getScenarioByName(name);

  getScenarios = () => this.scenariosList.getScenarios();

  navigateTo() { return browser.get('/eduapps/1/scenarios'); }

  getNewScenarioButton() { return element(by.css('button.mat-raised-button')); }

  confirmDialog() { element(by.cssContainingText('button', 'Ok')).click(); }
}
