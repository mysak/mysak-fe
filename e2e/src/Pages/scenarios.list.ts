import { by, element } from 'protractor';

export class ScenariosList {

  getScenarioByName(name: string) { return element(by.cssContainingText('tr', name)); }

  getScenarios() { return element.all(by.css('tr')); }

}
