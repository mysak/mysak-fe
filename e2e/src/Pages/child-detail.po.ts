import { browser, by, element } from 'protractor';
import { ScenariosList } from './scenarios.list';

export class ChildDetailPage {
  scenariosList: ScenariosList;

  constructor() { this.scenariosList = new ScenariosList(); }

  navigateTo(id: number) { return browser.get('/eduapps/1/children/' + id); }

  getSubmitButton() { return element(by.cssContainingText('button', 'Uložit')); }

  getScenarioByName = (name) => this.scenariosList.getScenarioByName(name);

  getQueueContent() { return element.all(by.css('.queue-card')); }

  getQueueCard(name: string) { return element(by.cssContainingText('.queue-card', name)); }

  confirmDialog() { element(by.cssContainingText('button', 'Ok')).click(); }

  getQueueTitles() { return this.getQueueContent().map(e => e.element(by.css('.mat-body')).getText()); }

  getInProgress() { return element(by.css('.in-progress-warning')).isPresent(); }
}
