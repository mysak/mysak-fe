import { browser, by, element } from 'protractor';

export class EduappsPage {
  navigateTo() { return browser.get('/eduapps'); }

  getTitleText() { return element(by.css('.mat-h1')).getText(); }

  getEduappByName(name: string) { return element(by.cssContainingText('button', name)); }
}
