import { ScenariosPage } from './Pages/scenarios.po';
import { browser, by, ElementFinder, ExpectedConditions } from 'protractor';
import { ScenarioDetailPage } from './Pages/scenario-detail.po';
import { dragAndDrop, matchUrl } from './helpers';
import { ToolbarNavigation } from './Pages/toolbar.po';

/**
 * TESTING FOLLOWING USE CASES:
 * UC1: Create scenarios
 * UC2: Edit scenarios
 * For detailed information on use cases please see documentation
 */
describe('scenario create and edit', () => {
  let scenariosPage: ScenariosPage;
  let scenarioDetailPage: ScenarioDetailPage;
  let toolbarNavigation: ToolbarNavigation;
  let newScenarioButton: ElementFinder;
  let queueContent: string[];
  let scenarioName: string;

  // Creates new Scenario
  beforeEach(() => {
    scenariosPage = new ScenariosPage();
    scenarioDetailPage = new ScenarioDetailPage();
    toolbarNavigation = new ToolbarNavigation();
    scenariosPage.navigateTo();
    newScenarioButton = scenariosPage.getNewScenarioButton();
    scenarioName = 'TESTING SCENARIO' + Date.now();

    // Scenario with given name doesn't exist yet
    expect(scenariosPage.getScenarioByName(scenarioName).isPresent()).toBeFalsy();

    // Create new Scenario
    queueContent = createScenario(scenarioName);
    expect(scenariosPage.getScenarioByName(scenarioName).isPresent()).toBeTruthy();
  });

  /**
   * Creates new scenario with the following content:
   * Name: given by parameter
   * Note: name + ' NOTE'
   * Queue:
   * +---------------------------------------------------------------------+
   * | Třídění kuliček | Auto a Loď | Na pláži | Kokosové ořechy | Dvojice |
   * +---------------------------------------------------------------------+
   * @param name: new scenario name
   * @return expected queue content
   */
  const createScenario = (name: string): string[] => {
    newScenarioButton.click();
    matchUrl('eduapps/1/scenarios/new');

    scenarioDetailPage.getNameInput().sendKeys(name);
    scenarioDetailPage.getNoteInput().sendKeys(name + ' NOTE');
    // Expand task 'Třídění'
    scenarioDetailPage.getTaskByName('Třídění').click();

    // Add few tasksettings
    scenarioDetailPage.getTaskSettingByName('Třídění kuliček').element(by.css('button')).click();
    scenarioDetailPage.getTaskSettingByName('Auto a Loď').element(by.css('button')).click();

    // Expand task 'Počáteční písmena'
    scenarioDetailPage.getTaskByName('Počáteční písmena').click();

    // Add few tasksettings
    scenarioDetailPage.getTaskSettingByName('Na pláži').element(by.css('button')).click();
    scenarioDetailPage.getTaskSettingByName('Kokosové ořechy').element(by.css('button')).click();

    // Add task 'Dvojice'
    scenarioDetailPage.getTaskByName('Dvojice').element(by.css('button')).click();

    scenarioDetailPage.getSubmitButton().click();
    matchUrl('eduapps/1/scenarios');

    return ['Třídění kuliček', 'Auto a Loď', 'Na pláži', 'Kokosové ořechy', 'Dvojice'];
  };

  /**
   * Verifies scenario with name given by parameter
   * @start Scenarios Page
   * @end Scenarios Page
   * @param name: to verify
   * @param note: to verify
   * @param expectedContent: queue content to verify
   */
  const verifyScenario = (name, note, expectedContent: string[]) => {
    const scenario = scenariosPage.getScenarioByName(name);
    // Wait for fetch from API if scenario was only recently created
    browser.wait(ExpectedConditions.presenceOf(scenario), 4000);
    scenario.click();

    // Should navigate to scenario detail
    matchUrl('eduapps\/1\/scenarios\/[0-9]*');

    expect(scenarioDetailPage.getNameInput().getAttribute('value')).toEqual(name);
    expect(scenarioDetailPage.getNoteInput().getAttribute('value')).toEqual(note);

    expect(scenarioDetailPage.getQueueTitles()).toEqual(expectedContent);

    toolbarNavigation.goToScenarios();
  };

  /**
   * Expects scenario with following content:
   * Name: given by parameter
   * Note: name + ' NOTE'
   * Queue:
   * +---------------------------------------------------------------------+
   * | Třídění kuliček | Auto a Loď | Na pláži | Kokosové ořechy | Dvojice |
   * +---------------------------------------------------------------------+
   * Modifies scenario (deletes 1 scenario, adds 1 scenario, swaps 2 scenarios)
   * Resulting content:
   * Name: given by parameter + ' EDIT'
   * Note: name + ' NOTE EDIT'
   * Queue:
   * +-----------------------------------------------------------------------------+
   * | Třídění kuliček | Na pláži | Auto a Loď | Kokosové ořechy | Třídění kuliček |
   * +-----------------------------------------------------------------------------+
   * @param name: edited scenario
   * @param saveAs: [experimental] save as different scenario
   */
  const editScenario = (name: string, saveAs?: string) => {
    scenariosPage.getScenarioByName(name).click();
    scenarioDetailPage.getNameInput().sendKeys(' EDIT');
    scenarioDetailPage.getNoteInput().sendKeys(' EDIT');
    // Expand task 'Třídění'
    scenarioDetailPage.getTaskByName('Třídění').click();

    // Add Třídění kuliček
    scenarioDetailPage.getTaskSettingByName('Třídění kuliček').element(by.css('button')).click();

    // Remove task 'Dvojice'
    scenarioDetailPage.getQueueCard('Dvojice').element(by.css('button')).click();

    // Swap tasks 'Na pláži' and 'Auto a Loď'
    const elem = scenarioDetailPage.getQueueCard('Na pláži').element(by.css('.queue-item-drag-handle'));
    const target = scenarioDetailPage.getQueueCard('Auto a Loď');
    dragAndDrop(elem, target);

    if (saveAs) {
      // WARNING: UNSTABLE
      scenarioDetailPage.saveAs(saveAs);
      toolbarNavigation.goToScenarios();
      browser.switchTo().alert().accept();
    } else {
      scenarioDetailPage.getSubmitButton().click();
      scenariosPage.confirmDialog();
    }
    const sep = browser.baseUrl.endsWith('/') ? '' : '/';
    // Workaround - wait for url to change
    browser.wait(ExpectedConditions.urlIs(browser.baseUrl + sep + 'eduapps/1/scenarios'), 1000);
    return ['Třídění kuliček', 'Na pláži', 'Auto a Loď', 'Kokosové ořechy', 'Třídění kuliček'];
  };

  it('should create new scenario correctly', () =>
    verifyScenario(scenarioName, scenarioName + ' NOTE', queueContent));

  it('should edit scenario correctly', () => {
    const newQueueContent = editScenario(scenarioName);
    matchUrl('eduapps/1/scenarios');

    verifyScenario(scenarioName + ' EDIT', scenarioName + ' NOTE EDIT', newQueueContent);
  });

  // Works only sometimes, unstable
  // it('should edit scenario and save as', () => {
  //   const newName = 'NEW NAME ' + Date.now();
  //   const newQueueContent = editScenario(scenarioName, newName);
  //   matchUrl('eduapps/1/scenarios');
  //
  //   // Verify old scenario;
  //   console.log(scenarioName);
  //   verifyScenario(scenarioName, scenarioName + ' NOTE', queueContent);
  //   // Verify new scenario
  //   verifyScenario(newName, scenarioName + ' NOTE EDIT', newQueueContent);
  // });


});
