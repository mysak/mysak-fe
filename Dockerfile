FROM node:13-alpine as build-stage

ARG PROJECT_PATH=/code

WORKDIR $PROJECT_PATH
COPY package.json $PROJECT_PATH
RUN npm install

COPY ./ $PROJECT_PATH

RUN npm run build -- --configuration production

FROM nginx:alpine

COPY --from=build-stage /code/nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /code/dist/mysak-fe /var/www

EXPOSE 80
STOPSIGNAL SIGINT

CMD echo "Starting the server..." && sed -i -e 's/$PORT/80/g' /etc/nginx/nginx.conf && nginx -g 'daemon off;'

