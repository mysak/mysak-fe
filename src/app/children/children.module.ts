import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChildrenRoutingModule } from './children-routing.module';
import { ChildrenPageComponent } from './children-page/children-page.component';
import { ChildDetailPageComponent } from './child-detail-page/child-detail-page.component';
import { SharedMaterialModule } from '../shared/shared-material/shared-material.module';
import { ChildrenListModule } from './shared/children-list/children-list.module';
import { ScenariosListModule } from '../scenarios/shared/scenarios-list/scenarios-list.module';
import { ChildDetailModule } from './shared/child-detail/child-detail.module';
import { ItemQueueModule } from '../shared/widgets/item-queue/item-queue.module';
import { ToolbarTitleModule } from '../shared/widgets/toolbar-title/toolbar-title.module';

@NgModule({
  imports: [
    CommonModule,
    ChildrenListModule,
    SharedMaterialModule,
    ChildrenRoutingModule,
    ScenariosListModule,
    ChildDetailModule,
    ItemQueueModule,
    ToolbarTitleModule
  ],
  declarations: [ChildrenPageComponent, ChildDetailPageComponent]
})
export class ChildrenModule {}
