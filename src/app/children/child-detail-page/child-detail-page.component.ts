import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Scenario } from '../../scenarios/scenario.model';
import { ChildService } from '../child.service';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { Child } from '../child.model';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupService } from '../../groups/group.service';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ScenarioQueueItem } from '../../scenarios/scenario-queue-item.model';
import { environment } from '../../../environments/environment';
import { ConfirmDialogComponent } from '../../shared/widgets/dialogs/confirm-dialog/confirm-dialog.component';
import { ComponentCanDeactivate } from '../../shared/guards/unsaved-changes-can-deactivate.guard';
import { QueueItem } from '../../shared/widgets/item-queue/queue-item.model';
import { GroupItem } from '../../groups/group-item';

/**
 * Page containing child info, list of scenarios and editable scenario queue
 * Uses childService and groupService to fetch data and update queue
 */
@Component({
  selector: 'app-child-detail-page',
  templateUrl: './child-detail-page.component.html',
  styleUrls: ['./child-detail-page.component.scss']
})
export class ChildDetailPageComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
  paginate = environment.PAGINATE;

  childId: number;
  child$: Observable<Child>;
  groupChips$: Observable<GroupItem[]>;
  queueLoading = true;
  scenarioQueueItems: ScenarioQueueItem[] = [];
  private subscriptions: Subscription = new Subscription();
  private firstInProgress: boolean;
  // indicator if save request is on the way
  saving = false;
  // indicator if user has unsaved changes
  dirty = false;
  firstQueueItem: ScenarioQueueItem;

  /**
   * @param route: used to get ID of child from URL
   * @param childService: used to get child info from API
   * @param groupService: used to get child's groups from API
   * @param router: used to navigate up one level on save
   * @param dialog: used to display confirmation dialog
   * @param snackBar: used to display message on successful save
   */
  constructor(private route: ActivatedRoute,
              private childService: ChildService,
              private groupService: GroupService,
              private router: Router,
              private dialog: MatDialog,
              public snackBar: MatSnackBar) {
  }


  /**
   * When scenario items change, set new items and mark dirty state true
   * @param items: new items
   */
  onItemsChange(items: QueueItem[]): void {
    this.scenarioQueueItems = items as ScenarioQueueItem[];
    this.dirty = true;
  }

  /**
   * Router guard, returns true if it is safe to deactivate route
   * @returns true if state is not dirty
   */
  @HostListener('window:beforeunload', ['$event'])
  canDeactivate(): boolean {
    return !this.dirty;
  }

  /**
   * OnInit: Get ID of child from URL,
   * get child from childService
   * get groups from GroupService
   */
  ngOnInit() {
    this.child$ = this.route.paramMap.pipe(
      switchMap(pMap => this.childService.getChild(+pMap.get('id'))),
      tap((ch: Child) => {
        // Remember child Id
        this.childId = ch.childId;
        this.queueLoading = false;
        if (ch.scenarioQueue && ch.scenarioQueue.scenarios) {
          const scenarios = ch.scenarioQueue.scenarios;
          // Save if first scenario is in progress
          this.firstInProgress = ch.scenarioQueue.front && ch.scenarioQueue.front.inProgress;
          // If first scenario is in progress, exclude it from queue
          this.scenarioQueueItems = (this.firstInProgress ? scenarios.slice(1) : scenarios)
            .map(s => new ScenarioQueueItem(s));
          // If first scenario is in progress, save it as firstQueueItem
          this.firstQueueItem = this.firstInProgress && scenarios.length > 0 ?
            new ScenarioQueueItem(scenarios[0]) :
            undefined;
        }
        // fetch user groups one by one
        if (ch && ch.groups) {
          this.groupChips$ = combineLatest(
            ch.groups.map(g => this.groupService.getGroup(g.groupId).pipe(
              map(group => new GroupItem(group))
            )));
        }
      }));
  }

  /**
   * Unsubscribe from subscribed observables
   */
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  /**
   * Called when user clicks on "Add" button next to scenario
   * Adds selected scenario to the back of child's queue
   * @param element: scenario to add to queue
   */
  onAddClick(element: Scenario): void {
    this.dirty = true;
    this.scenarioQueueItems.push(new ScenarioQueueItem(element));
  }

  /**
   * Called when user clicks save,
   * opens confirmation dialog. If successful, opens snackbar message
   */
  onSave(): void {
    this.subscriptions.add(
      // Open Confirm dialog
      this.dialog.open(ConfirmDialogComponent,
        {data: {title: 'Opravdu chcete uložit změny?'}}).afterClosed().pipe(
        // check if confirmed
        filter(confirmed => confirmed),
        // mark saving in progress
        tap(() => this.saving = true),
        // update child's queue
        switchMap(() => this.childService.updateScenarioQueue(this.childId,
          (this.firstInProgress ? [this.firstQueueItem] : []).concat(this.scenarioQueueItems))),
      ).subscribe(() => {
        // on successful update, mark reset flags
        this.saving = false;
        this.dirty = false;
        // on successful update, open snackbar message
        this.snackBar.open('Fronta scénářů úspěšně uložena',
          undefined,
          {duration: environment.SNACKBAR_DURATION});
        // on successful update, navigate up one level
        this.router.navigate(['../'], {relativeTo: this.route});
      }, err => {
        this.saving = false;
        console.error(err);
      }));
  }

  /**
   * Called when user clicks "Delete" button on first scenario item
   * Shows confirm dialog and cancels first scenario
   * WARNING: will cancel first scenario child has in queue, whether or not it is the one user clicked on
   *   To handle edge case, when child finishes his scenario while his queue is being edited,
   *   would require to check if the queue has changed, which can be done, however this should be preferably
   *   handled on server or made easier by including "lastChanged" attribute, best case would be if the queue
   *   was dynamically updated, however this would need severe changes in architecture and some kind of
   *   polling mechanism / webSocket, which is currently out of scope of this project
   *   TODO: FIX ISSUE WHEN USER FINISHES SCENARIO WHILE SOMEONE IS EDITING IT
   * @param scenario: scenario user clicked on
   */
  onFirstItemRemove(scenario: QueueItem): void {
    this.subscriptions.add(this.dialog.open(ConfirmDialogComponent, {
        data: {
          title: 'Opravdu chcete zrušit rozehraný scénář?',
          content: 'Scénář ' + scenario.item.name + ' bude zrušen'
        }
      }
    ).afterClosed().pipe(
      // check if confirmed
      filter(confirmed => confirmed),
      // cancel users first scenario - CAN CAUSE INCONSISTENCY IF QUEUE CHANGED IN THE MEANTIME
      switchMap(() => this.childService.cancelFirstScenario(this.childId)))
    // open snackbar on success
      .subscribe(() => {
        this.snackBar.open('Scénář zrušen',
          undefined,
          {duration: environment.SNACKBAR_DURATION});
        this.firstQueueItem = undefined;
        this.firstInProgress = false;
      }));
  }

  /**
   * Called when user clicks on "Clear queue" button
   * Sets QueueItems to empty array,
   * Doesn't clear currently running unfinished scenario
   */
  onClearQueueClick() {
    this.scenarioQueueItems = [];
  }
}
