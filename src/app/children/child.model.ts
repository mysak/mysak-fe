import { Group } from '../groups/group.model';
import { Scenario } from '../scenarios/scenario.model';
import { Image } from '../shared/models/image.model';

/**
 * Model of a Child Gender fetched from API
 */
export enum Gender {
  male = 'male',
  female = 'female',
}

/**
 * Model of a Child Scenario Queue fetched from API
 */
export class ScenarioQueue {
  front: {inProgress: boolean};
  scenarios: Scenario[];
}

/**
 * Model of a Child fetched from API
 */
export class Child {
  childId: number;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  image?: Image;
  gender: Gender;
  groups?: Group[];
  scenarioQueue?: ScenarioQueue;
}
