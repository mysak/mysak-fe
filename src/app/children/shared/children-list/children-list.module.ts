import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatListModule, MatSortModule, MatTableModule } from '@angular/material';
import { SharedMaterialModule } from '../../../shared/shared-material/shared-material.module';
import { ChildrenListComponent } from './children-list/children-list.component';
import { RouterModule } from '@angular/router';
import { UtilityModule } from '../../../shared/utility/utility.module';
import { ItemChipsModule } from '../../../shared/widgets/item-chips/item-chips.module';
import { DataFilteringModule } from '../../../shared/widgets/data-filtering/data-filtering.module';
import { FormsModule } from '@angular/forms';
import { ItemSelectModule } from '../../../shared/widgets/item-select/item-select.module';
import { DataTableModule } from '../../../shared/widgets/data-table/data-table.module';

@NgModule({
  declarations: [ChildrenListComponent],
  imports: [
    ItemSelectModule,
    CommonModule,
    RouterModule,
    UtilityModule,
    ItemChipsModule,
    DataFilteringModule,
    MatListModule,
    FormsModule,
    DataTableModule
  ],
  exports: [ ChildrenListComponent, MatTableModule, SharedMaterialModule, CommonModule, MatSortModule]
})
export class ChildrenListModule {}
