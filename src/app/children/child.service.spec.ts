import { TestBed } from '@angular/core/testing';

import { AssignmentType, ChildService } from './child.service';
import { ApiService } from '../shared/services/api.service';
import { TEST_CHILDREN } from '../shared/testing/data/Children';
import { of } from 'rxjs';
import { ChildrenOptions } from '../shared/models/collection-options';
import {
  observeResonse,
  testError,
  testHttpDelete,
  testHttpGet,
  testHttpPatch,
  testHttpPut
} from '../shared/testing/utility/service-utilities';
import { ScenarioQueueItem } from '../scenarios/scenario-queue-item.model';
import { TEST_SCENARIOS } from '../shared/testing/data/Scenarios';

describe('ChildService', () => {
  let childService: ChildService;
  let apiServiceSpy: jasmine.SpyObj<ApiService>;
  const child$ = of(TEST_CHILDREN[0]);
  const child = TEST_CHILDREN[0];
  const children$ = of(TEST_CHILDREN);
  const children = TEST_CHILDREN;
  const options: ChildrenOptions = {
    limit: 100,
    offset: 50,
    search: 'abc',
    sort: 'firstName',
    groupId: 1,
  };

  beforeEach(() => {
    const spy = jasmine.createSpyObj('ApiService', ['httpGet', 'httpPut', 'httpPatch', 'httpDelete']);
    TestBed.configureTestingModule(
      {
        providers: [
          ChildService,
          {provide: ApiService, useValue: spy},
        ]
      });
    childService = TestBed.get(ChildService);
    apiServiceSpy = TestBed.get(ApiService);
  });

  it('should be created', () => {
    expect(childService).toBeTruthy();
    expect(apiServiceSpy).toBeTruthy();
  });

  describe('#getChild', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(child$));

    it('should call httpGet with id of child', () =>
      testHttpGet(() => childService.getChild(42), apiServiceSpy.httpGet, '/children/42'));

    it('should return observable of child', () =>
      childService.getChild(1)
        .subscribe(res => expect(res).toEqual(child), fail));

    it('should return error on http error', () =>
      testError(() => childService.getChild(42), apiServiceSpy.httpGet));
  });

  describe('#getChildren', () => {

    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(children)));

    it('should call httpGet with url of /children', () =>
      testHttpGet(() => childService.getChildren(options),
        apiServiceSpy.httpGet,
        '/children',
        options,
      ));

    it('should return observable of children', () => {
      childService.getChildren().subscribe(res =>
        expect(res).toEqual(children), fail);
    });

    it('should return error on http error', () =>
      testError(() => childService.getChildren(), apiServiceSpy.httpGet));
  });

  describe('#getChildrenWithCount', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(children)));

    it('should call httpGet with url of /children', () =>
      testHttpGet(() => childService.getChildrenWithCount(options),
        apiServiceSpy.httpGet,
        '/children',
        options,
        true));

    it('should return observable of children and count', () =>
      childService.getChildrenWithCount().subscribe(res =>
        expect(res).toEqual({data: children, count: children.length}), fail));

    it('should return error on http error', () =>
      testError(() => childService.getChildrenWithCount(), apiServiceSpy.httpGet));
  });

  describe('#updateScenarioQueue', () => {
    const queueItems = TEST_SCENARIOS.map(s => new ScenarioQueueItem(s));
    const scenarios = TEST_SCENARIOS;

    beforeEach(() => apiServiceSpy.httpPut.and.returnValue(of({
      front: {inProgress: true}, scenarios
    })));

    it('should call httpPut with url of /children/{id}/scenario-queue', () =>
      testHttpPut(() => childService.updateScenarioQueue(42, queueItems),
        apiServiceSpy.httpPut,
        '/children/42/scenario-queue',
        queueItems.map(q => {
          return {scenarioId: q.id};
        })));

    it('should return observable of scenario queue', () => {
      childService.updateScenarioQueue(42, queueItems).subscribe(res =>
        expect(res).toEqual({front: {inProgress: true}, scenarios}), fail);
    });

    it('should return error on http error', () =>
      testError(() => childService.updateScenarioQueue(42, queueItems),
        apiServiceSpy.httpPut));

  });
  describe('#assignScenario', () => {
    const scenario = TEST_SCENARIOS[0];
    beforeEach(() => apiServiceSpy.httpPatch.and.returnValue(of(scenario)));

    it('should call httpPatch with url of /children/{id}/scenario-queue/front', () =>
      testHttpPatch(() => childService.assignScenario([42, 1, 666], scenario.scenarioId, AssignmentType.FRONT),
        apiServiceSpy.httpPatch,
        '/children/42,1,666/scenario-queue/front',
        {scenarioId: scenario.scenarioId}));

    it('should call httpPatch with url of /children/{id}/scenario-queue/back', () =>
      testHttpPatch(() => childService.assignScenario([42, 1, 666], scenario.scenarioId, AssignmentType.BACK),
        apiServiceSpy.httpPatch,
        '/children/42,1,666/scenario-queue/back',
        {scenarioId: scenario.scenarioId}));

    it('should return observable of scenario', () => {
      childService.assignScenario([42, 1, 666], scenario.scenarioId, AssignmentType.FRONT)
        .subscribe(res => expect(res).toEqual(scenario), fail);
      childService.assignScenario([42, 1, 666], scenario.scenarioId, AssignmentType.BACK)
        .subscribe(res => expect(res).toEqual(scenario), fail);
    });

    it('should return error on http error', () => {
      testError(() => childService.assignScenario([42, 1, 666], scenario.scenarioId, AssignmentType.FRONT),
        apiServiceSpy.httpPatch);
      testError(() => childService.assignScenario([42, 1, 666], scenario.scenarioId, AssignmentType.BACK),
        apiServiceSpy.httpPatch);
    });
  });

  describe('#cancelFirstScenario', () => {
    beforeEach(() => apiServiceSpy.httpDelete.and.returnValue(of()));

    it('should call httpDelete with url of /children/{id}/scenario-queue/front', () => {
      testHttpDelete(() => childService.cancelFirstScenario(42),
        apiServiceSpy.httpDelete,
        '/children/42/scenario-queue/front');
    });

    it('should return error on http error', () =>
      testError(() => childService.cancelFirstScenario(42), apiServiceSpy.httpGet));
  });
});
