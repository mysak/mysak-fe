import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EduappsPageComponent } from './eduapps-page/eduapps-page.component';
import { EduappDetailPageComponent } from './eduapp-detail-page/eduapp-detail-page.component';

/**
 * Routes:
 *   / --> EduappsPage
 *   /{eid} --> /{eid}/children
 *   /{eid}/children -- EduappsDetailPage --> LazyLoaded ChildrenModule
 *   /{eid}/scenarios -- EduappsDetailPage --> LazyLoaded ScenariosModule
 */
const routes: Routes = [
  {
    path: '',
    component: EduappsPageComponent,
    pathMatch: 'full'
  },
  {
    path: ':eid',
    component: EduappDetailPageComponent,
    children: [
      {
        path: '',
        redirectTo: 'children',
        pathMatch: 'full'
      },
      {
        path: 'children',
        loadChildren: '../children/children.module#ChildrenModule'
      },
      {
        path: 'scenarios',
        loadChildren: '../scenarios/scenarios.module#ScenariosModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EduappsRoutingModule {}

