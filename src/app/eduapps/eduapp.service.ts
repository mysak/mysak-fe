import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Eduapp } from './eduapp.model';
import { ApiService } from '../shared/services/api.service';

/**
 * Fetch eduapps from API using ApiService
 * Holds state of selected eduapp
 */
@Injectable({
  providedIn: 'root'
})
export class EduappService {
  eduAppUrl = '/eduapps';

  // Currently selected eduapp
  selectedEduappId: number;

  /**
   * Requires ApiService to fetch from API
   * @param apiService: uses this service to fetch from API
   */
  constructor(private apiService: ApiService) {
  }

  /**
   * Get all eduapps
   * Fetches /eduapps
   * @returns observable of fetched eduapps matching criteria
   */
  getEduapps(): Observable<Eduapp[]> {
    return this.apiService.httpGet<Eduapp[]>(this.eduAppUrl);
  }

  /**
   * @returns id of selected eduapp, undefined if no eduapp is selected
   */
  getSelectedId() {
    return this.selectedEduappId;
  }

  /**
   * Sets selected eduapp to the one given by parameter
   * @param selectedEduappId: eduapp to store as selected
   */
  setSelectedId(selectedEduappId: number) {
    this.selectedEduappId = selectedEduappId;
  }
}
