import { ChipItem } from '../shared/widgets/item-chips/chip-item.model';
import { SelectItem } from '../shared/widgets/item-select/select-item';
import { Eduapp } from './eduapp.model';

/**
 * Data class for material chip containing eduapp
 * Data class for material select option containing eduapp
 */
export class EduappItem implements ChipItem, SelectItem {
  image: string;
  id: number;
  label: string;
  constructor(e: Eduapp) {
    this.image = e.image ? e.image.resourceUri : undefined;
    this.label = e.name;
    this.id = e.eduappId;
  }
}
