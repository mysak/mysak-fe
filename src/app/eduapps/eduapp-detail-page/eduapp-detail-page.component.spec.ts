import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduappDetailPageComponent } from './eduapp-detail-page.component';

describe('EduappDetailPageComponent', () => {
  let component: EduappDetailPageComponent;
  let fixture: ComponentFixture<EduappDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduappDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduappDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
