import { TestBed } from '@angular/core/testing';

import { EduappService } from './eduapp.service';
import { ApiService } from '../shared/services/api.service';
import { TEST_EDUAPPS } from '../shared/testing/data/Eduapps';
import { of } from 'rxjs';
import { testError, testHttpGet } from '../shared/testing/utility/service-utilities';

describe('EduappService', () => {
  let eduappService: EduappService;
  let apiServiceSpy: jasmine.SpyObj<ApiService>;
  const eduapps$ = of(TEST_EDUAPPS);
  const eduapps = TEST_EDUAPPS;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('ApiService', ['httpGet']);
    TestBed.configureTestingModule(
      {
        providers: [
          EduappService,
          {provide: ApiService, useValue: spy},
        ]
      });
    eduappService = TestBed.get(EduappService);
    apiServiceSpy = TestBed.get(ApiService);
  });

  it('should be created', () => {
    expect(eduappService).toBeTruthy();
    expect(apiServiceSpy).toBeTruthy();
  });

  describe('#getEduapps', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(eduapps$));

    it('should call httpGet with url /eduapps', () =>
      testHttpGet(() => eduappService.getEduapps(), apiServiceSpy.httpGet, '/eduapps'));

    it('should return observable of eduapps', () => {
      eduappService.getEduapps().subscribe(res =>
        expect(res).toEqual(eduapps), fail);
    });

    it('should return error on http error', () => {
      testError(() => eduappService.getEduapps(), apiServiceSpy.httpGet);
    });
    describe('#getSelectedId and #setSelectedId', () => {
      it('should store and return the value', () => {
        expect(eduappService.getSelectedId()).toBeUndefined();
        eduappService.setSelectedId(42);
        expect(eduappService.getSelectedId()).toBe(42);
        eduappService.setSelectedId(0);
        expect(eduappService.getSelectedId()).toBe(0);
      });
    });
  });
});
