import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarModule } from './shared/widgets/toolbar/toolbar.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './shared/mocking/in-memory-data.service';
import { SharedMaterialModule } from './shared/shared-material/shared-material.module';
import { DialogsModule } from './shared/widgets/dialogs/dialogs.module';
import { httpInterceptorProviders } from './shared/interceptors/interceptor-providers';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    ToolbarModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    DialogsModule,
    SharedMaterialModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService,
      { apiBase: 'eduapps/1/', dataEncapsulation: false, delay: 400 }),
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule {}
