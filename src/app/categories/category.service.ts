import { Injectable } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Observable } from 'rxjs';
import { Category } from './category.model';
import { processCountHeader, toParams } from '../shared/services/service-utilities';
import { CategoryOptions } from '../shared/models/collection-options';
import { map } from 'rxjs/operators';

/**
 * Fetch categories from API using ApiService
 */
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private categoriesUrlSuffix = '/categories';

  /**
   * Requires ApiService to fetch from API
   * @param apiService: uses this service to fetch from API
   */
  constructor(private apiService: ApiService) {
  }

  /**
   * Get category with specific ID
   * Fetches /categories/{id} from API
   * @param id: category to fetch
   * @returns observable of fetched category matching id
   */
  getCategory(id: number): Observable<Category> {
    return this.apiService.httpGet<Category>(
      this.categoriesUrlSuffix + '/' + id,
    );
  }

  /**
   * Get categories matching selected options, if no options provided, get all categories
   * Fetches /categories?{options}
   * @param options: allows to specify which categories to retrieve, options are serialized into query params
   * @returns observable of fetched categories matching criteria
   */
  getCategories(options?: CategoryOptions): Observable<Category[]> {
    return this.getCategoriesWithCount(options).pipe(map(response => response.data));
  }

  /**
   * Get categories matching selected options, if no options provided, get all categories
   * Fetches /categories?{options}
   * @param options: allows to specify which categories to retrieve, options are serialized into query params
   * @returns observable of fetched categories matching criteria with total count of categories matching criteria
   * (with no respect to pagination)
   */
  getCategoriesWithCount(options?: CategoryOptions): Observable<{ data: Category[], count: number }> {
    return processCountHeader(this.apiService.httpGet<Category[]>(
      this.categoriesUrlSuffix,
      {params: toParams(options), observe: 'response'}
    ));
  }
}
