import { Image } from '../shared/models/image.model';
import { Task } from '../tasks/task.model';

/**
 * Model of a Category fetched from API
 */
export class Category {
  categoryId: number;
  name?: string;
  image?: Image;
  tasks?: Task[];
}
