import { TestBed } from '@angular/core/testing';

import { ApiService } from '../shared/services/api.service';
import { of } from 'rxjs';
import { observeResonse, testError, testHttpGet } from '../shared/testing/utility/service-utilities';
import { CategoryService } from './category.service';
import { TEST_CATEGORIES } from '../shared/testing/data/Categories';
import { CategoryOptions } from '../shared/models/collection-options';

describe('CategoryService', () => {
  let categoryService: CategoryService;
  let apiServiceSpy: jasmine.SpyObj<ApiService>;
  const category$ = of(TEST_CATEGORIES[0]);
  const categories$ = of(TEST_CATEGORIES);
  const category = TEST_CATEGORIES[0];
  const categories = TEST_CATEGORIES;
  const options: CategoryOptions = {
    limit: 100,
    offset: 50,
    search: 'abc',
    sort: 'name',
  };

  beforeEach(() => {
    const spy = jasmine.createSpyObj('ApiService', ['httpGet']);
    TestBed.configureTestingModule(
      {
        providers: [
          CategoryService,
          {provide: ApiService, useValue: spy},
        ]
      });
    categoryService = TestBed.get(CategoryService);
    apiServiceSpy = TestBed.get(ApiService);
  });

  it('should be created', () => {
    expect(categoryService).toBeTruthy();
    expect(apiServiceSpy).toBeTruthy();
  });

  describe('#getCategory', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(category$));

    it('should call httpGet with id of category', () =>
      testHttpGet(() => categoryService.getCategory(42), apiServiceSpy.httpGet, '/categories/42'));

    it('should return observable of category', () =>
      categoryService.getCategory(42)
        .subscribe(res => expect(res).toEqual(category), fail));

    it('should return error on http error', () =>
      testError(() => categoryService.getCategory(42), apiServiceSpy.httpGet));
  });

  describe('#getCategories', () => {

    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(categories)));

    it('should call httpGet with url of /categories', () =>
      testHttpGet(() => categoryService.getCategories(options),
        apiServiceSpy.httpGet,
        '/categories',
        options,
      ));

    it('should return observable of categories', () => {
      categoryService.getCategories().subscribe(res =>
        expect(res).toEqual(categories), fail);
    });

    it('should return error on http error', () =>
      testError(() => categoryService.getCategories(), apiServiceSpy.httpGet));
  });

  describe('#getCategoriesWithCount', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(categories)));

    it('should call httpGet with url of /categories', () =>
      testHttpGet(() => categoryService.getCategoriesWithCount(options),
        apiServiceSpy.httpGet,
        '/categories',
        options,
        true));

    it('should return observable of categories and count', () =>
      categoryService.getCategoriesWithCount().subscribe(res =>
        expect(res).toEqual({data: categories, count: categories.length}), fail));

    it('should return error on http error', () =>
      testError(() => categoryService.getCategoriesWithCount(), apiServiceSpy.httpGet));
  });

});
