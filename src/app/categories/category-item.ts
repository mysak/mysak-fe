import { ChipItem } from '../shared/widgets/item-chips/chip-item.model';
import { Category } from './category.model';
import { SelectItem } from '../shared/widgets/item-select/select-item';

/**
 * Data class for material chip containing category
 * Data class for material select option containing category
 */
export class CategoryItem implements ChipItem, SelectItem {
  constructor(c: Category) {
    this.id = c.categoryId;
    this.image = c.image ? c.image.resourceUri : undefined;
    this.label = c.name;
  }
  id: number;
  image: string;
  label: string;
}
