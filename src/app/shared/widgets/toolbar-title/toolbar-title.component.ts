import { Component, Input, OnInit } from '@angular/core';

/**
 * Title bar with secondary page title and a back button
 */
@Component({
  selector: 'app-toolbar-title',
  templateUrl: './toolbar-title.component.html',
  styleUrls: ['./toolbar-title.component.scss']
})
export class ToolbarTitleComponent implements OnInit {
  @Input() back = false;
  // route back (one level up by default)
  @Input() backRoute = '../';

  constructor() {
  }

  ngOnInit() {
  }

}
