import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarTitleComponent } from './toolbar-title.component';
import { click, RouterLinkDirectiveStub } from '../../testing/utility/component-utilities';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';

let component: TestHostComponent;
let fixture: ComponentFixture<TestHostComponent>;
let page: Page;

describe('ToolbarTitleComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RouterLinkDirectiveStub,
        ToolbarTitleComponent,
        TestHostComponent
      ],
      imports: [SharedMaterialModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    createComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(fixture).toBeTruthy();
    expect(page).toBeTruthy();
  });

  it('should attempt to navigate to ../ by default when clicked', () => {

    expect(page.backLink.navigatedTo).toBeNull('should not have navigated yet');
    click(page.buttonDe);

    fixture.detectChanges();

    expect(page.backLink.navigatedTo).toBe('../', 'should navigate to ../');
  });

  it('should attempt to navigate to backRoute when clicked', () => {
    component.backRoute = '/eduapps/42';
    component.passBackRoute = true;
    fixture.detectChanges();

    expect(page.backLink.navigatedTo).toBeNull('should not have navigated yet');
    click(page.buttonDe);
    fixture.detectChanges();

    expect(page.backLink.navigatedTo).toBe(component.backRoute, 'should navigate to ' + component.backRoute);
  });

  it('should hide back button when back is false', () => {
    component.back = false;
    fixture.detectChanges();
    expect(page.buttonDe).toBeUndefined();
    component.back = true;
    fixture.detectChanges();
    expect(page.buttonDe).toBeTruthy();
  });

  it('should show title', () => {
    expect(page.titleEl.textContent).toBe('TEST TITLE');
  });
});

// ----------------HELPERS-------------------

@Component({
  template: `
    <ng-container *ngIf="passBackRoute; else doNotPassBackRoute">
      <app-toolbar-title [back]="back" [backRoute]="backRoute">TEST TITLE</app-toolbar-title>
    </ng-container>
    <ng-template #doNotPassBackRoute>
      <app-toolbar-title [back]="back"><p>TEST TITLE</p></app-toolbar-title>
    </ng-template>`

})
class TestHostComponent {
  back = true;
  backRoute;
  passBackRoute = false;
}

/*
LEGAL NOTICE:
This code is heavily based on Angular demo code, which is governed by the MIT-style Licence:
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/

function createComponent() {
  fixture = TestBed.createComponent(TestHostComponent);
  component = fixture.componentInstance;
  page = new Page(fixture);

  // 1st change detection triggers ngOnInit
  fixture.detectChanges();
}


// https://angular.io/guide/testing#use-a-page-object
class Page {
  get buttonDe() {
    return this.toolbarTitleDe.queryAll(By.css('button'))[0];
  }
  get backLink() { return this.buttonDe.injector.get(RouterLinkDirectiveStub); }
  get toolbarTitleDe() { return this.fixture.debugElement.query(By.css('app-toolbar-title')); }
  get titleEl() { return this.fixture.nativeElement.querySelector('p'); }

  private fixture: ComponentFixture<TestHostComponent>;

  constructor(compFixture: ComponentFixture<TestHostComponent>) {
    this.fixture = compFixture;
  }
}
