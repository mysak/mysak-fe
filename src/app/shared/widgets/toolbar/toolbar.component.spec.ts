import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ToolbarComponent } from './toolbar.component';
import { Router } from '@angular/router';
import { EduappService } from '../../../eduapps/eduapp.service';
import { EduappServiceSpy } from '../../testing/services/eduapp-service-spy';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { MatDividerModule, MatSelectChange } from '@angular/material';
import { ItemSelectStubComponent } from '../../testing/stubs/item-select.stub.component';
import { click, RouterLinkDirectiveStub } from '../../testing/utility/component-utilities';
import { By } from '@angular/platform-browser';
import { TEST_EDUAPPS } from '../../testing/data/Eduapps';
import { EduappItem } from '../../../eduapps/eduapp-item';

let component: ToolbarComponent;
let fixture: ComponentFixture<ToolbarComponent>;
let page: Page;
const SELECTED_EDUAPP = 2;

describe('ToolbarComponent', () => {

  beforeEach(async(() => {
    const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
    TestBed.configureTestingModule({
      declarations: [
        RouterLinkDirectiveStub,
        ToolbarComponent,
        ItemSelectStubComponent
      ],
      imports: [SharedMaterialModule, MatDividerModule],
      providers: [
        {provide: EduappService, useClass: EduappServiceSpy},
        {provide: Router, useValue: routerSpy},
      ],
    }).compileComponents();
  }));

  beforeEach(async(() => {
    createComponent();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(fixture).toBeTruthy();
    expect(page).toBeTruthy();
  });

  describe('navigation', () => {

    describe('navigation links', () => {
      it('should have three links with correct urls', () => {
        expect(page.links.length).toEqual(3);
        expect(page.homeLink.linkParams).toEqual('/', 'home link should be /');
        expect(page.childrenLink.linkParams)
          .toEqual('/eduapps/2/children', 'children link should be /eduapps/:eid/children');
        expect(page.scenariosLink.linkParams)
          .toEqual('/eduapps/2/scenarios', 'scenarios link should be /eduapps/:eid/scenarios');
      });

      testNavigateOnClick('/', 'homeLinkDe');
      testNavigateOnClick('/eduapps/' + SELECTED_EDUAPP + '/children', 'childrenLinkDe');
      testNavigateOnClick('/eduapps/' + SELECTED_EDUAPP + '/scenarios', 'scenariosLinkDe');
    });


    it('should navigate when different eduapp was selected', fakeAsync(() => {
      const selectMethodSpy = spyOn(component, 'onEduappSelect').and.callThrough();
      const navigateSpy: jasmine.Spy = page.routerSpy.navigateByUrl;
      navigateSpy.and.returnValue(Promise.resolve(true));
      page.routerUrl = '/eduapps/1/children';
      expect(page.routerSpy.url).toEqual(page.routerUrl);

      page.eduappSelectStub._selectId(42);

      // onEduappSelect should be called once with ID 42
      expect(selectMethodSpy).toHaveBeenCalledTimes(1);
      expect(selectMethodSpy).toHaveBeenCalledWith(new MatSelectChange(page.eduappSelectStub.matSelectSpy, 42));

      // Testing workaround for reloading when Eduapp changes
      // First, navigateByUrl should be called once with url '/'
      expect(navigateSpy).toHaveBeenCalledTimes(1);
      expect(navigateSpy.calls.mostRecent().args[0]).toBe('/');

      tick();

      // When resolved, should be called again with correct url
      expect(navigateSpy).toHaveBeenCalledTimes(2);
      expect(navigateSpy.calls.mostRecent().args[0]).toBe('/eduapps/42/children');
      // Should resolve correctly so Eduapp Id should not be changed afterwards
      expect(page.eduappSelectStub.matSelectSpy.writeValue).toHaveBeenCalledTimes(0);

    }));

    it('should reset eduapp Id if navigation failed', fakeAsync(() => {
      const navigateSpy: jasmine.Spy = page.routerSpy.navigateByUrl;
      navigateSpy.and.returnValue(Promise.resolve(false));
      page.routerUrl = '/eduapps/1/children';
      expect(page.routerSpy.url).toEqual(page.routerUrl);

      page.eduappSelectStub._selectId(42);

      // Testing workaround for reloading when Eduapp changes
      // First, navigateByUrl should be called once with url '/'
      expect(navigateSpy).toHaveBeenCalledTimes(1);
      expect(navigateSpy.calls.mostRecent().args[0]).toBe('/');

      tick();

      // Navigation should fail so selected eduapp should be restored to previous value
      expect(page.eduappSelectStub.matSelectSpy.writeValue).toHaveBeenCalledTimes(1);
      expect(page.eduappSelectStub.matSelectSpy.writeValue).toHaveBeenCalledWith(SELECTED_EDUAPP);
    }));

    describe('should fall back to list url if on eduapp specific page', () => {
      navigateToListTest('children');
      navigateToListTest('scenarios');
    });

  });

  describe('Eduapp selection', () => {
    it('should send eduapp items to item select\'s input', () => {
      const items = page.eduappSelectStub.items;
      const expected = TEST_EDUAPPS.map(e => new EduappItem(e));
      // Test if EduappItem constructor works correctly
      expect(expected.map(e => e.id)).toEqual(TEST_EDUAPPS.map(e => e.eduappId));
      expect(expected.map(e => e.label)).toEqual(TEST_EDUAPPS.map(e => e.name));
      expect(expected.map(e => e.image)).toEqual(TEST_EDUAPPS.map(e => e.image.resourceUri));
      // Test if items passed to item-select match
      expect(items).toEqual(expected);
    });

    it('should pass selected id to item-selct', () => {
      const itemSelect = page.eduappSelectStub;

      expect(itemSelect.initialItemId).toBe(SELECTED_EDUAPP,
        'initial value should be selected eduapp' + SELECTED_EDUAPP);
    });
  });

});


// ----------------HELPERS-------------------

function navigateToListTest(listName: string) {
  return it('should navigate to ' + listName + ' list on eduapp change if url was /eduapps/:id/' + listName + '/1',
    fakeAsync(() => {
      const navigateSpy: jasmine.Spy = page.routerSpy.navigateByUrl;
      navigateSpy.and.returnValue(Promise.resolve(true));
      page.routerUrl = '/eduapps/1/' + listName + '/1';
      expect(page.routerSpy.url).toEqual(page.routerUrl);

      page.eduappSelectStub._selectId(42);

      // Testing workaround for reloading when Eduapp changes
      // First, navigateByUrl should be called once with url '/'
      expect(navigateSpy).toHaveBeenCalledTimes(1);
      expect(navigateSpy.calls.mostRecent().args[0]).toBe('/');

      tick();

      // When resolved, should be called again with correct url
      expect(navigateSpy).toHaveBeenCalledTimes(2);
      expect(navigateSpy.calls.mostRecent().args[0]).toBe('/eduapps/42/' + listName);

      // Should resolve correctly so Eduapp Id should not be changed afterwards
      expect(page.eduappSelectStub.matSelectSpy.writeValue).toHaveBeenCalledTimes(0);
    }));
}

/*
LEGAL NOTICE:
This code is heavily based on Angular demo code, which is governed by the MIT-style Licence:
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/


function testNavigateOnClick(url, deName: string) {
  return it('should attempt to navigate to ' + url + ' when clicked', () => {
    const routerLinkDirective = page[deName].injector.get(RouterLinkDirectiveStub);

    expect(routerLinkDirective.navigatedTo).toBeNull('should not have navigated yet');
    click(page[deName]);
    fixture.detectChanges();

    expect(routerLinkDirective.navigatedTo).toBe(url, 'should navigate to ' + url);
  });

}

function createComponent() {
  fixture = TestBed.createComponent(ToolbarComponent);
  component = fixture.componentInstance;
  page = new Page(fixture);
  page.eduappServiceSpy.setSelectedId(SELECTED_EDUAPP);

  // 1st change detection triggers ngOnInit
  fixture.detectChanges();
  return fixture.whenStable().then(() => {
    // 2nd change detection displays the async-fetched hero
    fixture.detectChanges();
  });
}

// https://angular.io/guide/testing#use-a-page-object
class Page {
  get anchorDes() {
    return this.fixture.debugElement.queryAll(By.directive(RouterLinkDirectiveStub));
  }
  get links() {
    return this.anchorDes.map(de => de.injector.get(RouterLinkDirectiveStub));
  }
  get homeLink() { return this.links[0]; }
  get homeLinkDe() { return this.anchorDes[0]; }
  get childrenLink() { return this.links[1]; }
  get childrenLinkDe() { return this.anchorDes[1]; }
  get scenariosLink() { return this.links[2]; }
  get scenariosLinkDe() { return this.anchorDes[2]; }
  eduappSelectStub: ItemSelectStubComponent;

  routerSpy: jasmine.SpyObj<Router>;
  routerUrl: string;
  eduappServiceSpy: jasmine.SpyObj<EduappService>;
  private fixture: ComponentFixture<ToolbarComponent>;

  constructor(compFixture: ComponentFixture<ToolbarComponent>) {
    // get the navigate spy from the injected router spy object
    this.routerSpy = <any>compFixture.debugElement.injector.get(Router);
    this.eduappServiceSpy = <any>compFixture.debugElement.injector.get(EduappService);

    this.routerUrl = '/';
    // Workaround because of readonly 'url' property
    Object.defineProperty(this.routerSpy, 'url', {get: () => this.routerUrl});
    this.routerSpy.navigateByUrl.and.returnValue(
      new Promise(resolve => setTimeout(() => resolve())));

    this.fixture = compFixture;
    this.eduappSelectStub =
      this.fixture.debugElement.query(By.css('app-item-select')).componentInstance;
  }

  //// query helpers ////
  private query<T>(selector: string): T {
    return this.fixture.nativeElement.querySelector(selector);
  }

  private queryAll<T>(selector: string): T[] {
    return this.fixture.nativeElement.querySelectorAll(selector);
  }
}
