import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemQueueComponent } from './item-queue.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { MatCardModule } from '@angular/material';

@NgModule({
  declarations: [ItemQueueComponent],
  imports: [
    CommonModule,
    SharedMaterialModule,
    DragDropModule,
    MatCardModule,
  ],
  exports: [
    ItemQueueComponent
  ]
})
export class ItemQueueModule {}
