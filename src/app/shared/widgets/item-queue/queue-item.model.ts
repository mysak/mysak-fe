export interface QueueItem {
  id: number;
  image: string;
  // primary label
  label: string;
  // secondary label
  secondary?: string;
  item?: any;
  // true if item cannot be dragged
  dragDisabled?: boolean;
  // true if item cannot be removed
  removeDisabled?: boolean;
}
