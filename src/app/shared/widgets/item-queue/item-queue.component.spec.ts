import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemQueueComponent } from './item-queue.component';

describe('ItemQueueComponent', () => {
  let component: ItemQueueComponent;
  let fixture: ComponentFixture<ItemQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
