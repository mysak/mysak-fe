import { NgModule } from '@angular/core';
import { ItemSelectComponent } from './item-select.component';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { MatOptionModule, MatSelectModule } from '@angular/material';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [ItemSelectComponent],
  imports: [
    SharedMaterialModule,
    MatSelectModule,
    CommonModule
  ],
  exports: [
    SharedMaterialModule,
    MatSelectModule,
    MatOptionModule,
    ItemSelectComponent,
    CommonModule
  ]
})
export class ItemSelectModule { }
