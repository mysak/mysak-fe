import { TemplateRef } from '@angular/core';

// Default column template: only Label and Values
export interface DataTableColumn {
  label?: string;
  propName: string;
  // true if sorting is enabled
  sort?: boolean;
}

// Custom column with custom html template
// Id must match sorting prop name!
export interface DataTableCustomColumn {
  label?: string;
  id: string;
  template: TemplateRef<any>;
  // true if sorting is enabled
  sort?: boolean;
}
