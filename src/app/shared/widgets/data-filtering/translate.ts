import { MatPaginatorIntl } from '@angular/material';

/**
 * Paginator translation to czech
 */
export class MatPaginatorIntlCz extends MatPaginatorIntl {
  itemsPerPageLabel = 'Položek';
  nextPageLabel = 'Další';
  previousPageLabel = 'Předchozí';
  getRangeLabel = (page: number, pageSize: number, length: number) =>
    (page * pageSize + 1) + ' - ' +
    Math.min((page + 1) * pageSize, length) + ' z ' + length

}
