import { DialogData } from '../dialog-data';

export interface ErrorDialogData extends DialogData {
  title: string;
  content?: string;
}
