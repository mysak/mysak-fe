import { DialogData } from '../dialog-data';

export interface InputDialogData extends DialogData {
  title: string;
  content?: string;
  error?: boolean;
  // Input field configuration
  input: DialogInput;
}

export interface DialogInput {
  hint: string;
  // Initial input value
  initial?: string;
  // True if dialog can be saved if the input field is same as initial
  sameInputAllowed?: boolean;
  // Error message if user attempts to confirm dialog without changing the input
  sameInputError?: string;
  // Error message if user attempts to confirm dialog with empty input
  emptyInputError?: string;
  // Maximum length of input field
  maxLength?: number;
}

