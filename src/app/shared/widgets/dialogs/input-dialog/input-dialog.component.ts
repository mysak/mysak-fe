import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { InputDialogData } from './input-dialog-data';

/** Error state matcher from official angular material documentation
 * https://material.angular.io/components/input/examples
 * Error when invalid control is dirty, touched, or submitted.*/
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

/**
 * Displays dialog with an input field to ask user for input
 */
@Component({
  selector: 'app-input-dialog',
  templateUrl: './input-dialog.component.html',
  styleUrls: ['./input-dialog.component.scss']
})
export class InputDialogComponent implements OnInit {

  input = '';
  errorStateMatcher = new MyErrorStateMatcher();

  /**
   * @param dialogRef: used to close dialog with inputted data as return value
   * @param data: dialog title, content and input field configuration
   */
  constructor(
    public dialogRef: MatDialogRef<InputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: InputDialogData) {
  }

  /**
   * Set input to initial value
   */
  ngOnInit() {
    this.input = this.data.input.initial ? this.data.input.initial : '';
  }

}
