import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmDialogData } from './confirm-dialog-data';

/**
 * Displays config dialog with Ok or Cancel options
 * MatDialog's afterClosed() returns true if user clicked Ok
 */
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  /**
   * @param data: injected on component creation, contains dialog title and content
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData) {
  }

  ngOnInit() {
  }

}
