import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterLinkDirectiveStub } from '../../testing/utility/component-utilities';
import { SharedMaterialModule } from '../../shared-material/shared-material.module';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { ItemChipsComponent } from './item-chips.component';
import { ChipItem } from './chip-item.model';

let component: TestHostComponent;
let fixture: ComponentFixture<TestHostComponent>;
let page: Page;
const testItems: ChipItem[] = [
  {image: '#1_Image', id: 1, label: '#1'},
  {image: '#2_Image', id: 1, label: '#2'},
  {image: '#3_Image', id: 1, label: '#3'}];

describe('ItemChipsComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RouterLinkDirectiveStub,
        ItemChipsComponent,
        TestHostComponent
      ],
      imports: [SharedMaterialModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    createComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(fixture).toBeTruthy();
    expect(page).toBeTruthy();
  });

  it('should show test Items', () => {
    expect(page.itemDes.map(de => de.nativeElement.textContent)).toEqual(testItems.map(ch => ch.label));
  });

  it('should show test Item images', () => {
    page.itemImages.map(de => de.nativeElement.src).forEach(
      (src, idx) => expect(src).toContain(testItems[idx].image)
    );
  });
  it('should show test Item images', () => {
    page.itemImages.map(de => de.nativeElement.src).forEach(
      (src, idx) => expect(src).toContain(testItems[idx].image)
    );
  });

});

// ----------------HELPERS-------------------

@Component({
  template: `
    <app-item-chips [items]="items"></app-item-chips>`
})
class TestHostComponent {
  items = testItems;
}

/*
LEGAL NOTICE:
This code is heavily based on Angular demo code, which is governed by the MIT-style Licence:
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/

function createComponent() {
  fixture = TestBed.createComponent(TestHostComponent);
  component = fixture.componentInstance;
  page = new Page(fixture);

  // 1st change detection triggers ngOnInit
  fixture.detectChanges();
}


// https://angular.io/guide/testing#use-a-page-object
class Page {
  get itemDes() {
    return this.fixture.debugElement.queryAll(By.css('mat-chip'));
  }

  get itemLabels() {
    return this.itemDes.map(i => i.query(By.css('span')));
  }

  get itemImages() {
    return this.itemDes.map(i => i.query(By.css('img')));
  }

  get titleEl() { return this.fixture.nativeElement.querySelector('p'); }

  private fixture: ComponentFixture<TestHostComponent>;

  constructor(compFixture: ComponentFixture<TestHostComponent>) {
    this.fixture = compFixture;
  }
}

