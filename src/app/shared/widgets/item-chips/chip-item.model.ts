export interface ChipItem {
  image?: string;
  label: string;
  id: number;
}
