import { Component, Input, OnInit } from '@angular/core';
import { ChipItem } from './chip-item.model';

/**
 * Ribbon with small material design chip items containing image and label
 */
@Component({
  selector: 'app-item-chips',
  templateUrl: './item-chips.component.html',
  styleUrls: ['./item-chips.component.scss']
})
export class ItemChipsComponent implements OnInit {
  // Input items
  @Input() items: ChipItem[];

  constructor() {
  }

  ngOnInit() {
  }

}
