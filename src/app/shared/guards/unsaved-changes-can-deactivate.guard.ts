import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

export interface ComponentCanDeactivate {
  canDeactivate(): boolean;
}

/**
 * Router guard whose mission is to not allow leaving the site when user has unsaved changes
 */
@Injectable({
  providedIn: 'root'
})
export class UnsavedChangesCanDeactivateGuard implements CanDeactivate<ComponentCanDeactivate> {
  /**
   * Checks if user has saved all changes, if not, display a confirmation dialog before unloading the page
   * @returns true if it is safe to leave the site
   */
  canDeactivate(component: ComponentCanDeactivate,
                currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot,
                nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.canDeactivate() ? true : confirm('Opustit stránku? Veškeré změny budou ztraceny.');
  }
}
