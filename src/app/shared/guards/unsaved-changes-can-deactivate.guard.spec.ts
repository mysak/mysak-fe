import { TestBed, async, inject } from '@angular/core/testing';

import { UnsavedChangesCanDeactivateGuard } from './unsaved-changes-can-deactivate.guard';

describe('UnsavedChangesCanDeactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnsavedChangesCanDeactivateGuard]
    });
  });

  it('should ...', inject([UnsavedChangesCanDeactivateGuard], (guard: UnsavedChangesCanDeactivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
