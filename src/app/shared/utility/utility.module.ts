import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgePipe } from './pipes/age.pipe';
import { GenderPipe } from './pipes/gender.pipe';

@NgModule({
  declarations: [AgePipe, GenderPipe],
  imports: [
    CommonModule
  ],
  exports: [
    AgePipe,
    GenderPipe,
    CommonModule
  ]
})
export class UtilityModule { }
