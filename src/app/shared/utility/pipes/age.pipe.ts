import { Pipe, PipeTransform } from '@angular/core';

/**
 * Transform birthday in UTC format into integer representing age
 */
@Pipe({
  name: 'age'
})
export class AgePipe implements PipeTransform {

  /**
   * @param value: UTC format, i.e. '2013-08-05',
   * @param args: optional args, not used
   * @returns integer value of age
   */
  transform(value: string, args?: any): number {
    const birthday = new Date(value);
    const now = new Date();
    let diff = now.getFullYear() - birthday.getFullYear();
    // check if current day and month is past birthday
    diff -= now.getMonth() >= birthday.getMonth() && now.getDay() >= birthday.getDay() ? 0 : 1;
    return diff;
  }

}
