export const TEST_GROUPS = [
  {
    groupId: 1,
    name: 'Medvídci',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/1286/1286599.svg',
      mimeType: 'image/jpeg',
      fileSize: 33200,
      createdOn: '2017-11-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }, {
    groupId: 2,
    name: 'Berušky',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/809/809133.svg',
      mimeType: 'image/jpeg',
      fileSize: 63200,
      createdOn: '2017-12-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }
];
