import { Category } from '../../../categories/category.model';

export const TEST_CATEGORIES: Category[] = [
    {
      categoryId: 1,
      name: 'Řeč',
      image: {
        resourceUri: 'https://image.flaticon.com/icons/svg/512/512500.svg',
        mimeType: 'image/jpeg',
        fileSize: 12405,
        createdOn: '2018-10-01T12:50:08Z',
        dimensions: {
          width: 320,
          height: 240
        }
      }
    }, {
    categoryId: 2,
    name: 'Zrak',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/149/149099.svg',
      mimeType: 'image/jpeg',
      fileSize: 47635,
      createdOn: '2018-04-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }, {
    categoryId: 3,
    name: 'Sluch',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/1377/1377941.svg',
      mimeType: 'image/jpeg',
      fileSize: 40508,
      createdOn: '2018-02-05T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }];
