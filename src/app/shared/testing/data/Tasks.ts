import { Task } from '../../../tasks/task.model';

export const TEST_TASKS: Task[] =
  [
    {
      taskId: 1,
      name: 'Třídění',
      categories: [
        {
          categoryId: 1
        }, {
          categoryId: 2
        }
      ],
      image: {
        resourceUri: 'https://image.flaticon.com/icons/svg/138/138839.svg',
        mimeType: 'image/jpeg',
        fileSize: 32100,
        createdOn: '2018-02-01T12:50:08Z',
        dimensions: {
          width: 320,
          height: 240
        }
      }
    }, {
    taskId: 2,
    name: 'Počáteční písmena',
    categories: [
      {
        categoryId: 1
      }, {
        categoryId: 3
      }
    ],
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/138/138839.svg',
      mimeType: 'image/jpeg',
      fileSize: 10394,
      createdOn: '2017-12-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }];
