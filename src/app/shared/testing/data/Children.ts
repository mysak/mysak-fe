import { Child, Gender } from '../../../children/child.model';
import { TEST_SCENARIOS } from './Scenarios';

export const TEST_CHILDREN: Child[] = [
  {
    'childId': 1,
    'firstName': 'Miluše',
    'lastName': 'Kosová',
    'dateOfBirth': '2013-08-05',
    'gender': Gender.female,
    'groups': [
      {
        'groupId': 1
      }, {
        'groupId': 2
      }
    ],
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/163/163847.svg',
      mimeType: 'image/jpeg',
      fileSize: 62985,
      createdOn: '2018-11-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
    scenarioQueue: {
      front: {inProgress: true},
      scenarios: [TEST_SCENARIOS[1], TEST_SCENARIOS[2]]
    }
  }, {
    'childId': 2,
    'firstName': 'Václav',
    'lastName': 'Hrstka',
    'dateOfBirth': '2014-02-01',
    'gender': Gender.male,
    'groups': [
      {
        'groupId': 2
      }
    ],
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/163/163801.svg',
      mimeType: 'image/jpeg',
      fileSize: 62985,
      createdOn: '2018-11-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
    scenarioQueue: {
      front: {inProgress: true},
      scenarios: [TEST_SCENARIOS[1], TEST_SCENARIOS[2], TEST_SCENARIOS[4]]
    }
  }, {
    'childId': 3,
    'firstName': 'Blanka',
    'lastName': 'Tkáčová',
    'dateOfBirth': '2012-09-15',
    'gender': Gender.female,
    'groups': [
      {
        'groupId': 2
      }
    ],
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/163/163847.svg',
      mimeType: 'image/jpeg',
      fileSize: 62985,
      createdOn: '2018-11-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
    scenarioQueue: {
      front: {inProgress: false},
      scenarios: [TEST_SCENARIOS[1], TEST_SCENARIOS[2], TEST_SCENARIOS[3]]
    }
  }, {
    'childId': 4,
    'firstName': 'Augustýn',
    'lastName': 'Gregor',
    'dateOfBirth': '2014-03-09',
    'gender': Gender.male,
    'groups': [
      {
        'groupId': 1
      }, {
        'groupId': 2
      }
    ],
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/163/163801.svg',
      mimeType: 'image/jpeg',
      fileSize: 62985,
      createdOn: '2018-11-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
  }, {
    'childId': 5,
    'firstName': 'Blažej',
    'lastName': 'Kaňok',
    'dateOfBirth': '2013-08-05',
    'gender': Gender.male,
    'groups': [
      {
        'groupId': 1
      }
    ],
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/163/163801.svg',
      mimeType: 'image/jpeg',
      fileSize: 62985,
      createdOn: '2018-11-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    },
  }
];
