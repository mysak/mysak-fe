export const TEST_EDUAPPS = [
  {
    eduappId: 1,
    name: 'Myšák',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/166/166745.svg',
      mimeType: 'image/jpeg',
      fileSize: 88900,
      createdOn: '2017-11-01T12:50:08Z',
      dimensions: {
        width: 640,
        height: 480
      }
    }
  }, {
    eduappId: 2,
    name: 'Tam a sem',
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/166/166745.svg',
      mimeType: 'image/jpeg',
      fileSize: 88900,
      createdOn: '2017-11-01T12:50:08Z',
      dimensions: {
        width: 640,
        height: 480
      }
    }
  }
];
