import { TaskSetting } from '../../../tasks/tasksetting.model';

export const TEST_TASKSETTINGS: TaskSetting[] = [
  {
    taskSettingId: 1,
    taskId: 1,
    name: 'Třídění kuliček',
    difficulty: 1,
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/138/138591.svg',
      mimeType: 'image/jpeg',
      fileSize: 17635,
      createdOn: '2018-11-30T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }, {
    taskSettingId: 2,
    taskId: 1,
    name: 'Auto a Loď',
    difficulty: 2,
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/138/138591.svg',
      mimeType: 'image/jpeg',
      fileSize: 17635,
      createdOn: '2018-11-11T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }, {
    taskSettingId: 3,
    taskId: 1,
    name: 'Zahrada',
    difficulty: 3,
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/138/138591.svg',
      mimeType: 'image/jpeg',
      fileSize: 22314,
      createdOn: '2016-12-01T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }, {
    taskSettingId: 4,
    taskId: 1,
    name: 'Barevné míčky',
    difficulty: 4,
    image: {
      resourceUri: 'https://image.flaticon.com/icons/svg/138/138591.svg',
      mimeType: 'image/jpeg',
      fileSize: 32104,
      createdOn: '2018-11-21T12:50:08Z',
      dimensions: {
        width: 320,
        height: 240
      }
    }
  }
];
