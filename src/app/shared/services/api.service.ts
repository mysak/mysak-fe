import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DialogErrorStrategy, ErrorStrategy, SnackBarErrorStrategy } from './error-strategy';
import { MatDialog, MatSnackBar } from '@angular/material';

/**
 * Configuration for handling errors by API service
 * Message is text passed to strategy.handleError()
 */
export interface ErrorConfig {
  message?: string;
  strategy?: ErrorStrategy;
}

/**
 * This error is raised if no eduapp is selected,
 * but attempt to get eduapp-specific data was made
 */
export class InvalidEduappError implements Error {
  message: 'Missing eduapp Id';
  name: 'Invalid Eduapp';
}

/**
 * Base API service that transparently handles errors
 * All HTTP requests should be made through this service,
 * to configure error handling, one can pass an ErrorConfig to ApiService methods
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // Default error handling strategies for each type of request
  DEFAULT_GET_ERR: ErrorStrategy;
  DEFAULT_POST_ERR: ErrorStrategy;
  DEFAULT_PUT_ERR: ErrorStrategy;
  DEFAULT_DELETE_ERR: ErrorStrategy;
  DEFAULT_PATCH_ERR: ErrorStrategy;
  INVALID_EDUAPP_ERR: ErrorStrategy;

  /**
   * Construct service and set default error strategies:
   * GET, DELETE -> error is displayed via snackbar
   * POST, PUT, PATCH -> error is displayed via dialog (generally greater damage if one of these fails)
   * @param http: uses HttpClient to make actual requests
   * @param snackbar: used to display snackbar error messages
   * @param dialog: used to display dialog error messages
   */
  constructor(private http: HttpClient,
              private snackbar: MatSnackBar,
              private dialog: MatDialog) {
    this.DEFAULT_GET_ERR = this.DEFAULT_DELETE_ERR = new SnackBarErrorStrategy(snackbar);
    this.DEFAULT_POST_ERR = this.DEFAULT_PUT_ERR = this.DEFAULT_PATCH_ERR = new DialogErrorStrategy(dialog);
    this.INVALID_EDUAPP_ERR
      = new DialogErrorStrategy(dialog, {title: 'Chyba', content: 'Není zvolen Eduapp'});
  }

  /**
   * Handle missing selected eduapp and possible http errors,
   * If no error strategy is provided, use default strategies for error handling
   * (dialog for get request, popup for post, etc...)
   * @param request http request and returns observable of T
   * @param defaultErrorStrategy default strategy to use if err.strategy is not provided
   * @param err contains message, default value and error strategy. IF NO VALUE IS PROVIDED, THE ERROR STOPS PROPAGATING,
   * if no strategy is provided, the default is used
   */
  private handleError<T>(request: Observable<T>,
                         defaultErrorStrategy: ErrorStrategy,
                         err?: ErrorConfig): Observable<T> {
    return request.pipe(
      // TODO: Add retry logic
      // TODO: Check out this cool project: https://www.npmjs.com/package/backoff-rxjs
      // timeout(2500),
      // retry(2),
      catchError(e => {
        if (e instanceof InvalidEduappError) {
          return this.INVALID_EDUAPP_ERR.handleError(e);
        }
        return err && err.strategy ?
          err.strategy.handleError(e, err.message) :
          defaultErrorStrategy.handleError(e, err ? err.message : undefined);
      })
    );
  }


  /**
   * Pass GET request to http client and handle error on the way
   * @param url: request url
   * @param options: request options
   * @param err: error config to change error handling behaviour
   */
  httpGet<T>(url: string, options?: { observe?: 'body', params?: HttpParams, headers?: HttpHeaders },
             err?: ErrorConfig): Observable<T>;

  httpGet<T>(url: string, options?: { observe: 'response', params?: HttpParams, headers?: HttpHeaders },
             err?: ErrorConfig): Observable<HttpResponse<T>>;

  httpGet<T>(url: string, options?: object | null, err?: ErrorConfig): Observable<HttpResponse<T> | T> {
    return this.handleError<T>(this.http.get<T>(url, options), this.DEFAULT_GET_ERR, err);
  }

  /**
   * Pass PUT request to http client and handle error on the way
   * @param url: request url
   * @param options: request options
   * @param err: error config to change error handling behaviour
   */
  httpPut<T>(url: string, body: any | null, options?: { observe?: 'body', params?: HttpParams, headers?: HttpHeaders },
             err?: ErrorConfig): Observable<T>;

  httpPut<T>(url: string, body: any | null, options?: { observe: 'response', params?: HttpParams, headers?: HttpHeaders },
             err?: ErrorConfig): Observable<HttpResponse<T>>;

  httpPut<T>(url: string, body: any | null, options?: object | null,
             err?: ErrorConfig): Observable<HttpResponse<T> | T> {
    return this.handleError<T>(this.http.put<T>(url, body, options), this.DEFAULT_PUT_ERR, err);
  }

  /**
   * Pass PATCH request to http client and handle error on the way
   * @param url: request url
   * @param options: request options
   * @param err: error config to change error handling behaviour
   */
  httpPatch<T>(url: string, body: any | null, options?: { observe?: 'body', params?: HttpParams, headers?: HttpHeaders },
               err?: ErrorConfig): Observable<T>;

  httpPatch<T>(url: string, body: any | null, options?: { observe: 'response', params?: HttpParams, headers?: HttpHeaders },
               err?: ErrorConfig): Observable<HttpResponse<T>>;

  httpPatch<T>(url: string, body: any | null, options?: object | null,
               err?: ErrorConfig): Observable<HttpResponse<T> | T> {
    return this.handleError<T>(this.http.patch<T>(url, body, options), this.DEFAULT_PATCH_ERR, err);
  }

  /**
   * Pass POST request to http client and handle error on the way
   * @param url: request url
   * @param options: request options
   * @param err: error config to change error handling behaviour
   */
  httpPost<T>(url: string, body: any | null, options?: { observe?: 'body', params?: HttpParams, headers?: HttpHeaders },
              err?: ErrorConfig): Observable<T>;

  httpPost<T>(url: string, body: any | null, options?: { observe: 'response', params?: HttpParams, headers?: HttpHeaders },
              err?: ErrorConfig): Observable<HttpResponse<T>>;

  httpPost<T>(url: string, body: any | null, options?: object | null,
              err?: ErrorConfig): Observable<HttpResponse<T> | T> {
    return this.handleError<T>(this.http.post<T>(url, body, options), this.DEFAULT_POST_ERR, err);
  }


  /**
   * Pass DELETE request to http client and handle error on the way
   * @param url: request url
   * @param options: request options
   * @param err: error config to change error handling behaviour
   */
  httpDelete<T>(url: string, options?: { observe?: 'body', params?: HttpParams, headers?: HttpHeaders },
                err?: ErrorConfig): Observable<T>;

  httpDelete<T>(url: string, options?: { observe: 'response', params?: HttpParams, headers?: HttpHeaders },
                err?: ErrorConfig): Observable<HttpResponse<T>>;

  httpDelete<T>(url: string, options?: object | null,
                err?: ErrorConfig): Observable<HttpResponse<T> | T> {
    return this.handleError<T>(this.http.delete<T>(url, options), this.DEFAULT_DELETE_ERR, err);
  }
}
