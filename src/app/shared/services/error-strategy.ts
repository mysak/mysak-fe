import { Observable, throwError } from 'rxjs';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ErrorDialogComponent } from '../widgets/dialogs/error-dialog/error-dialog.component';
import { DialogData } from '../widgets/dialogs/dialog-data';
import { ErrorDialogData } from '../widgets/dialogs/error-dialog/error-dialog-data';
import { environment } from '../../../environments/environment';

/**
 * Error strategy determines how ApiService handles errors
 */
export interface ErrorStrategy {
  /**
   * This method should be called if an error that user should be notified about occurs
   * @param e: error that occurred
   * @param message: error message that should be displayed to the user
   */
  handleError(e: Error, message?: string): Observable<never>;
}

export abstract class ErrorStrategyImpl implements ErrorStrategy {
  protected defaultErrorMessage: any;

  /**
   * Takes default error message as a parameter when constructing ErrorStrategy,
   * all errors then have this message if no message argument is provided in handleError method
   * @param defaultErrorMessage: use this message for all errors
   * unless message argument of handleError method is specified
   */
  protected constructor(defaultErrorMessage?: any) {
    if (defaultErrorMessage) {
      this.defaultErrorMessage = defaultErrorMessage;
    }
  }

  abstract handleError(e: Error, message?: string): Observable<never>;
}

/**
 * Handles error by displaying an error dialog to user
 */
export class DialogErrorStrategy extends ErrorStrategyImpl {
  protected dialog: MatDialog;

  /**
   * @param dialog: used to display message to user (not injected automatically)
   * @param defaultErrorMessage: can be used to specify title of the dialog, since
   * handleError method only takes message string
   */
  constructor(dialog: MatDialog, defaultErrorMessage?: DialogData) {
    super(defaultErrorMessage);
    if (!this.defaultErrorMessage) {
      // Default message if none was provided
      this.defaultErrorMessage = {title: 'Chyba', content: 'Něco se pokazilo'};
    }
    this.dialog = dialog;
  }

  /**
   * Open dialog displaying message given as parameter if an error occurs
   * @param e: error that occurred
   * @param message: error message that should be displayed to the user
   */
  handleError(e: Error, message?: string): Observable<never> {
    const data: ErrorDialogData = {
      // Use title from default message
      title: this.defaultErrorMessage.title,
      // If message provided use it as content, otherwise use defaultErrorMessage content
      content: message ? message : this.defaultErrorMessage.content
    };
    // Use setTimeout as a workaround for ExpressionChangedAfterItHasBeenCheckedError
    setTimeout(() => this.dialog.open(ErrorDialogComponent, {data}));
    // rethrow error
    return throwError(e);
  }
}

/**
 * Handles error by displaying an error snackbar to user
 */
export class SnackBarErrorStrategy extends ErrorStrategyImpl {
  protected snackbar: MatSnackBar;

  /**
   * @param snackbar: used to display message to user (not injected automatically)
   * @param defaultErrorMessage: default message displayed to the user
   */
  constructor(snackbar: MatSnackBar, defaultErrorMessage?: string) {
    super(defaultErrorMessage);
    if (!this.defaultErrorMessage) {
      // Default message if none was provided
      this.defaultErrorMessage = 'Něco se pokazilo';
    }
    this.snackbar = snackbar;
  }

  /**
   * Open snackbar displaying message given as parameter if an error occurs
   * @param e: error that occurred
   * @param message: error message that should be displayed to the user
   */
  handleError(e: Error, message?: string): Observable<never> {
    // Use default message if no message is provided
    // Use setTimeout as a workaround for ExpressionChangedAfterItHasBeenCheckedError
    setTimeout(() =>
      this.snackbar.open(message ? message : this.defaultErrorMessage, undefined,
        {duration: environment.SNACKBAR_DURATION}));
    // Rethrow error
    return throwError(e);
  }
}

/**
 * Do not display any message, only rethrow error
 */
export class NoMessageErrorStrategy implements ErrorStrategy {
  handleError(e: Error, message?: string): Observable<never> {
    // Rethrow error
    return throwError(e);
  }
}
