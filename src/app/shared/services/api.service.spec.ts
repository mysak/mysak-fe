import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { ApiService, InvalidEduappError } from './api.service';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatDialog, MatSnackBar } from '@angular/material';
import { SpyType, testHandleError } from '../testing/utility/service-utilities';
import { throwError } from 'rxjs';

interface ITestData {
  answerToLifeTheUniverseAndEverything: number;
}

describe('ApiService', () => {
  let apiService: ApiService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let dialogSpy: jasmine.SpyObj<MatDialog>;
  let snackbarSpy: jasmine.SpyObj<MatSnackBar>;
  const httpParams = new HttpParams({fromObject: {testParam: 'str', query: '42'}});

  const getMethod = (httpMethod: string): string => {
    switch (httpMethod.toLowerCase()) {
      case 'get' :
        return 'httpGet';
      case 'put' :
        return 'httpPut';
      case 'post' :
        return 'httpPost';
      case 'patch' :
        return 'httpPatch';
      case 'delete' :
        return 'httpDelete';
    }
  };

  const shouldCallHttpClient = <T extends CallableFunction>(
    httpMethod: string, url: string, body: object, params?: HttpParams, observes?: boolean) => {

    const methodName = getMethod(httpMethod);
    const testData = {answerToLifeTheUniverseAndEverything: 42};

    it('should call HttpClient ' + methodName + ' method ' +
      (params ? 'with params' : '') +
      (observes ? 'and observe response' : ''), () => {

      const options = (params || observes) && {params, observe: observes ? 'response' : undefined};

      const request$ = body ?
        apiService[methodName]<ITestData>(url, body, options) :
        apiService[methodName]<ITestData>(url, options);

      request$.subscribe(observes ?
        (res: HttpResponse<ITestData>) => expect(res.body).toEqual(testData) :
        (res: ITestData) => expect(res).toEqual(testData),
        () => fail('Expected succesful response'));


      const req = httpTestingController.expectOne({
        method: httpMethod.toUpperCase(),
        url: params ? url + '?' + params.toString() : url,
      });
      if (body) {
        expect(req.request.body).toEqual(body);
      }

      req.flush(testData);
    });
  };

  const specDefinitions = <T extends CallableFunction>(methodName: string) => {
    const body = ['post', 'put', 'patch'].includes(methodName.toLowerCase()) ?
      {joke: ['hip', 'hip'] /* (hip hip array!)*/} :
      undefined;

    shouldCallHttpClient(methodName, '/testUrl', body, httpParams);
    shouldCallHttpClient(methodName, '/testUrl', body);
    shouldCallHttpClient(methodName, '/testUrl', body, httpParams, true);
    shouldCallHttpClient(methodName, '/testUrl', body, httpParams);
    it('should catch, handle and rethrow Error', fakeAsync(() => {
      // snackbar for get and delete, dialog for post, put and patch (more important)
      const spyType = ['post', 'put', 'patch'].includes(methodName.toLowerCase()) ? SpyType.DIALOG : SpyType.SNACKBAR;
      testHandleError(body ?
        (message: string) => apiService[getMethod(methodName)]<ITestData>('/testUrl', body, undefined, {message}) :
        (message: string) => apiService[getMethod(methodName)]<ITestData>('/testUrl', undefined, {message}),
        httpTestingController,
        spyType === SpyType.DIALOG ? dialogSpy : snackbarSpy,
        spyType,
        {method: methodName, url: '/testUrl'}
      );
    }));
  };

  beforeEach(() => {
    const dialogSpyDep = jasmine.createSpyObj('MatDialog', ['open']);
    const snackbarSpyDep = jasmine.createSpyObj('MatSnackbar', ['open']);
    TestBed.configureTestingModule(
      {
        imports: [HttpClientTestingModule],
        providers: [
          ApiService,
          {provide: MatDialog, useValue: dialogSpyDep},
          {provide: MatSnackBar, useValue: snackbarSpyDep},
        ]
      });
    apiService = TestBed.get(ApiService);
    httpClient = TestBed.get(HttpClient);
    snackbarSpy = TestBed.get(MatSnackBar);
    dialogSpy = TestBed.get(MatDialog);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(apiService).toBeTruthy();
    expect(snackbarSpy).toBeTruthy();
    expect(dialogSpy).toBeTruthy();
    expect(httpClient).toBeTruthy();
    expect(httpTestingController).toBeTruthy();
  });


  describe('#httpGet', () => specDefinitions('GET'));
  describe('#httpPut', () => specDefinitions('PUT'));
  describe('#httpPost', () => specDefinitions('POST'));
  describe('#httpPatch', () => specDefinitions('PATCH'));
  describe('#httpDelete', () => specDefinitions('DELETE'));
});

describe('ApiService InvalidEduappError', () => {
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let dialogSpy: jasmine.SpyObj<MatDialog>;
  let apiService: jasmine.SpyObj<ApiService>;
  beforeEach(() => {
    const dialogSpyDep = jasmine.createSpyObj('MatDialog', ['open']);
    const snackbarSpyDep = jasmine.createSpyObj('MatSnackbar', ['open']);
    const spy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'post', 'patch', 'delete']);
    TestBed.configureTestingModule(
      {
        providers: [
          ApiService,
          {provide: HttpClient, useValue: spy},
          {provide: MatDialog, useValue: dialogSpyDep},
          {provide: MatSnackBar, useValue: snackbarSpyDep}
        ]
      });
    httpClientSpy = TestBed.get(HttpClient);
    dialogSpy = TestBed.get(MatDialog);
    apiService = TestBed.get(ApiService);
    dialogSpy = TestBed.get(MatDialog);
  });

  it('should catch, handle and rethrow InvaldEduappError', fakeAsync(() => {
    const error = new InvalidEduappError();
    httpClientSpy.get.and.returnValue(throwError(error));
    apiService.httpGet<ITestData>('/testUrl').subscribe(
      () => fail('expected an error'),
      e => expect(e).toBe(error));
    tick();
    expect(dialogSpy.open).toHaveBeenCalledTimes(1);
    expect(dialogSpy.open.calls.mostRecent().args[1].data.content).toBe('Není zvolen Eduapp');
  }));
});
