import * as faker from 'faker';
import { Category } from '../../categories/category.model';
import { Group } from '../../groups/group.model';
import { Image } from '../models/image.model';
import { Task } from '../../tasks/task.model';
import { TaskSetting } from '../../tasks/tasksetting.model';
import { Scenario } from '../../scenarios/scenario.model';
import { Eduapp } from '../../eduapps/eduapp.model';
import { Child, Gender, ScenarioQueue } from '../../children/child.model';

const GROUP_TEMPLATES = [
  {id: 1, name: 'Berušky', image: 'assets/ladybug.png'}, // Berušky
  {id: 2, name: 'Medvídci', image: 'assets/bear.png'} // Medvídci
];

const CATEGORY_TEMPLATES = [
  {id: 1, name: 'Řeč', image: 'assets/talking.png'},
  {id: 2, name: 'Zrak', image: 'assets/sight.png'},
  {id: 3, name: 'Sluch', image: 'assets/hearing.png'},
  {id: 4, name: 'Myšlení', image: 'assets/idea.png'},
];

const TASK_IMAGE = 'assets/image-files.png';

const TASK_SETTINGS_IMAGE = 'assets/image.png';

const TASK1_TASK_SETTINGS = [
  {id: 1, name: 'Třídění kuliček', image: TASK_SETTINGS_IMAGE},
  {id: 2, name: 'Auto a Loď', image: TASK_SETTINGS_IMAGE},
  {id: 3, name: 'Zahrada', image: TASK_SETTINGS_IMAGE},
  {id: 4, name: 'Barevné míčky', image: TASK_SETTINGS_IMAGE},
];

const TASK2_TASK_SETTINGS = [
  {id: 5, name: 'Zvířata u moře', image: TASK_SETTINGS_IMAGE},
  {id: 6, name: 'Na pláži', image: TASK_SETTINGS_IMAGE},
  {id: 7, name: 'Kokosové ořechy', image: TASK_SETTINGS_IMAGE},
  {id: 8, name: 'Ve vzduchu a ve vodě', image: TASK_SETTINGS_IMAGE},
];

const TASK3_TASK_SETTINGS = [
  {id: 9, name: 'Cukroví', image: TASK_SETTINGS_IMAGE},
  {id: 10, name: 'Z čeho se vyrábí?', image: TASK_SETTINGS_IMAGE},
  {id: 11, name: 'Husa', image: TASK_SETTINGS_IMAGE},
];

const TASK4_TASK_SETTINGS = [
  {id: 12, name: 'Deky', image: TASK_SETTINGS_IMAGE},
  {id: 13, name: 'Strom', image: TASK_SETTINGS_IMAGE},
  {id: 14, name: 'Slunečníky', image: TASK_SETTINGS_IMAGE},
];

const TASK_TEMPLATES = [
  {id: 1, name: 'Třídění', categories: [1, 2], image: TASK_IMAGE, taskSettings: TASK1_TASK_SETTINGS},
  {id: 2, name: 'Počáteční písmena', categories: [1, 3], image: TASK_IMAGE, taskSettings: TASK2_TASK_SETTINGS},
  {id: 3, name: 'Dvojice', categories: [4], image: TASK_IMAGE, taskSettings: TASK3_TASK_SETTINGS},
  {id: 4, name: 'Poznej Objekt', categories: [4], image: TASK_IMAGE, taskSettings: TASK4_TASK_SETTINGS},
];

const EDUAPP_TEMPLATES = [
  {id: 1, name: 'Myšák', image: 'assets/mouse.png'},
  // {id: 2, name: 'Tam a Sem', image: 'https://image.flaticon.com/icons/svg/166/166745.svg'}
];

export const SCENARIO_IMAGE = 'assets/task.png';

const SCENARIO_TEMPLATES = [
  {id: 1, name: 'Zrak pro Medvídky', image: SCENARIO_IMAGE},
  {id: 2, name: 'Motorika 1', image: SCENARIO_IMAGE},
  {id: 3, name: 'Motorika 2', image: SCENARIO_IMAGE},
  {id: 4, name: 'Řeč - lehčí', image: SCENARIO_IMAGE},
  {id: 5, name: 'Řeč - těžší', image: SCENARIO_IMAGE},
  {id: 6, name: 'Zrak 1', image: SCENARIO_IMAGE},
  {id: 7, name: 'Zrak 2', image: SCENARIO_IMAGE},
];

const FEMALE_IMAGE = 'assets/girl.png';
const MALE_IMAGE = 'assets/user.png';
const CHILDREN_COUNT = 40;

export function generateData() {
  faker.locale = 'cz';
  faker.seed(42);
  const eduapps = generateEduapps(EDUAPP_TEMPLATES);
  const groups = generateGroups(GROUP_TEMPLATES);
  const categories = generateCategories(CATEGORY_TEMPLATES);
  const tasks = generateTasks(TASK_TEMPLATES);
  const scenarios = generateScenarios(SCENARIO_TEMPLATES, tasks);
  const children = generateChildren(scenarios, groups.map(g => ({groupId: g.groupId})), CHILDREN_COUNT);
  return {groups, categories, tasks, eduapps, children, scenarios};
}

export const createImage = (uri: string): Image => ({
  resourceUri: uri, mimeType: 'image/jpeg', fileSize: 33200, createdOn: '2017-11-01T12:50:08Z',
  dimensions: {width: 320, height: 240}
});

const generateEduapps = (templates: { id: number, name: string, image: string }[]): Eduapp[] =>
  templates.map(e => ({eduappId: e.id, name: e.name, image: createImage(e.image)}));

const generateGroups = (templates: { id: number, name: string, image: string }[]): Group[] =>
  templates.map(g => ({groupId: g.id, name: g.name, image: createImage(g.image)}));

const generateCategories = (templates: { id: number, name: string, image: string }[]): Category[] =>
  templates.map(c => ({categoryId: c.id, name: c.name, image: createImage(c.image)}));

const generateTasks = (templates: {
  id: number,
  name: string,
  categories: number[],
  image: string,
  taskSettings: { id: number, name: string, image: string }[]
}[]): Task[] =>
  templates.map(t => ({
    taskId: t.id, name: t.name, image: createImage(t.image),
    categories: t.categories.map(cid => ({categoryId: cid})),
    taskSettings: generateTaskSettings(t.id, t.taskSettings)
  }));

const generateTaskSettings = (taskId: number, templates: { id: number, name: string, image: string }[]): TaskSetting[] =>
  templates.map(ts =>
    ({
      taskSettingId: ts.id,
      name: ts.name,
      taskId,
      image: createImage(ts.image),
      difficulty: faker.random.number({min: 1, max: 5})
    }));

const generateScenarios = (templates: { id: number, name: string, image: string }[], tasks: Task[]): Scenario[] =>
  templates.map(s => ({
    scenarioId: s.id,
    name: s.name,
    lastChange: faker.date.past(1).toISOString(),
    note: '',
    image: createImage(s.image),
    items: generateScenarioItems(tasks, faker.random.number(TASK_TEMPLATES.length), 0.5),
  }));

const generateScenarioItems = (tasks: Task[], count, taskProbability: number):
  { type: 'task' | 'taskSetting', data: Task | TaskSetting }[] => {
  const res = [];
  for (let i = 0; i < count; i++) {
    const task = faker.random.arrayElement(tasks);
    const isTaskSetting = rBool(taskProbability);
    isTaskSetting ?
      res.push({type: 'taskSetting', data: faker.random.arrayElement(task.taskSettings)}) :
      res.push({type: 'task', data: task});
  }
  return res;
};

const generateChildren = (scenarios: Scenario[], groups: { groupId: number }[], count: number): Child[] =>
  Array.from(Array(count).keys()).map(
    i => {
      const name: string[] = faker.name.findName().split(' ').filter(n => !n.includes('.'));
      const firstName = name[0];
      name.splice(0, 1);
      let lastName = name.join(' ');
      lastName = lastName[0].toUpperCase() + lastName.slice(1);

      const gender = lastName.includes('ová') ? Gender.female : Gender.male;
      return {
        gender, firstName, lastName,
        childId: i + 1,
        dateOfBirth: faker.date.between('2013-12-31', '2015-01-01').toISOString(),
        image: createImage(gender === Gender.female ? FEMALE_IMAGE : MALE_IMAGE),
        groups: getRandomElements(groups, 1),
        scenarioQueue: generateScenarioQueue(scenarios, faker.random.number(SCENARIO_TEMPLATES.length))
      };
    });

const generateScenarioQueue = (scenarios: Scenario[], count: number): ScenarioQueue =>
  ({
    front: {inProgress: count > 1 ? rBool(0.3) : false},
    scenarios: getRandomElements(scenarios, count)
  });

// --------- HELPERS ----------
const rBool = (probability: number): boolean =>
  faker.random.number({min: 0, max: 1, precision: 0.01}) <= probability;

const getRandomElements = <T>(array: T[], count: number): T[] => {
  const tmpArray = Object.assign([], array);
  if (count >= array.length) { return tmpArray; }
  const res = [];
  for (let i = 0; i < count; i++) {
    const randomIndex = faker.random.number(tmpArray.length - 1);
    res.push(tmpArray[randomIndex]);
    tmpArray.splice(randomIndex, 1);
  }
  return res;
};
