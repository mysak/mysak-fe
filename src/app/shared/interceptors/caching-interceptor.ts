import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { startWith, tap } from 'rxjs/operators';
import { CacheService } from '../services/cache.service';
import { environment } from '../../../environments/environment';

/*
LEGAL NOTICE:
This code is heavily based on Angular demo code, which is governed by the
MIT-style Licence:
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/

/**
 * Determine if a request is cacheable based on environment variables
 * @param req: request to check
 * @returns true if request is cacheable
 */
function isCachable(req: HttpRequest<any>): boolean {
  if (req.method !== 'GET' || !environment.CACHEABLE) { return false; }

  // remove api base from url to compare with environment values
  let reqUrl = req.url.replace(environment.apiBase, '');
  // remove api base and eduapp ID from url to compare with environment values
  reqUrl = reqUrl.replace(/\/eduapps\/[0-9]*\//, '/');
  return environment.CACHEABLE
    .map(url => typeof url === 'string' ?
      url === reqUrl :
      url.test(reqUrl))
    .reduce((acc, current) => acc || current, false);
}

/**
 * Intercepting requests and storing them to cache if they're cacheable.
 * Uses Cache Service to store the results in memory
 */
@Injectable()
export class CachingInterceptor implements HttpInterceptor {
  /**
   * @param cache: used to store responses
   */
  constructor(private cache: CacheService) {
  }

  /**
   * Intercepts the request and adds it to the cache if cacheable
   * Cache then refetch strategy is applied if request doesn't contain x-no-refresh header,
   * If request contains x-no-refresh header, the header is removed and cache or fetch strategy is applied
   * @param req: request to intercept
   * @param next: next handler
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!isCachable(req)) { return next.handle(req); }

    const cachedResponse = this.cache.get(req);

    console.log('CACHEABLE: [cache' + (cachedResponse ? '' : ' not') + ' available]: ' + req.urlWithParams);

    const results$ =
      next.handle(
        // remove x-no-refresh header
        req.clone({headers: req.headers.delete('x-no-refresh')}))
        .pipe(
          // save to cache
          tap(event => event instanceof HttpResponse && this.cache.put(req, event))
        );

    // cache-or-fetch
    if (req.headers.get('x-no-refresh')) {
      return cachedResponse ?
        of(cachedResponse) : results$;
    }

    // cache-then-refetch by default
    return cachedResponse ?
      results$.pipe(startWith(cachedResponse)) :
      results$;
  }
}

