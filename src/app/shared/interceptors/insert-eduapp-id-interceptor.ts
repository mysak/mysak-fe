import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { EduappService } from '../../eduapps/eduapp.service';
import { environment } from '../../../environments/environment';
import { InvalidEduappError } from '../services/api.service';

/**
 * This interceptor inserts API Base and selected eduapp id to URL of all requests except for requests on /eduapps URL
 */
@Injectable()
export class InsertEduappIdInterceptor implements HttpInterceptor {
  /**
   * @param eduappService: used to obtain selected eduapp ID
   */
  constructor(private eduappService: EduappService) {
  }

  /**
   * Intercepts the request and adds /eduapps/{id} to its url
   * @param req: request to intercept
   * @param next: next handler
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const selectedId = this.eduappService.getSelectedId();
    if (selectedId === undefined && req.url !== '/eduapps') {
      console.error(req);
      return throwError(new InvalidEduappError());
    }
    // construct new url, add eduapp ID if the request is not for '/eduapps'
    const newUrl = environment.apiBase +
      (req.url === '/eduapps' ? '' : '/eduapps/' + selectedId) + req.url;
    return next.handle(req.clone({url: newUrl}));
  }
}
