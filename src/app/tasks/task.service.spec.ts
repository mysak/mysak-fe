import { TestBed } from '@angular/core/testing';

import { ApiService } from '../shared/services/api.service';
import { of } from 'rxjs';
import { observeResonse, testError, testHttpGet } from '../shared/testing/utility/service-utilities';
import { TaskService } from './task.service';
import { TEST_TASKS } from '../shared/testing/data/Tasks';
import { TaskOptions } from '../shared/models/collection-options';
import { TEST_TASKSETTINGS } from '../shared/testing/data/TaskSettings';

describe('TaskService', () => {
  let taskService: TaskService;
  let apiServiceSpy: jasmine.SpyObj<ApiService>;
  const task$ = of(TEST_TASKS[0]);
  const taskSettings = TEST_TASKSETTINGS;
  const task = TEST_TASKS[0];
  const tasks = TEST_TASKS;
  const options: TaskOptions = {
    limit: 100,
    offset: 50,
    search: 'abc',
    sort: 'name',
  };

  beforeEach(() => {
    const spy = jasmine.createSpyObj('ApiService', ['httpGet']);
    TestBed.configureTestingModule(
      {
        providers: [
          TaskService,
          {provide: ApiService, useValue: spy},
        ]
      });
    taskService = TestBed.get(TaskService);
    apiServiceSpy = TestBed.get(ApiService);
  });

  it('should be created', () => {
    expect(taskService).toBeTruthy();
    expect(apiServiceSpy).toBeTruthy();
  });

  describe('#getTask', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(task$));

    it('should call httpGet with id of task', () =>
      testHttpGet(() => taskService.getTask(42), apiServiceSpy.httpGet, '/tasks/42'));

    it('should return observable of task', () =>
      taskService.getTask(42)
        .subscribe(res => expect(res).toEqual(task), fail));

    it('should return error on http error', () =>
      testError(() => taskService.getTask(42), apiServiceSpy.httpGet));
  });

  describe('#getTasks', () => {

    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(tasks)));

    it('should call httpGet with url of /tasks', () =>
      testHttpGet(() => taskService.getTasks(options),
        apiServiceSpy.httpGet,
        '/tasks',
        options,
      ));

    it('should return observable of tasks', () => {
      taskService.getTasks().subscribe(res =>
        expect(res).toEqual(tasks), fail);
    });

    it('should return error on http error', () =>
      testError(() => taskService.getTasks(), apiServiceSpy.httpGet));
  });

  describe('#getTasksWithCount', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(tasks)));

    it('should call httpGet with url of /tasks', () =>
      testHttpGet(() => taskService.getTasksWithCount(options),
        apiServiceSpy.httpGet,
        '/tasks',
        options,
        true));

    it('should return observable of tasks and count', () =>
      taskService.getTasksWithCount().subscribe(res =>
        expect(res).toEqual({data: tasks, count: tasks.length}), fail));

    it('should return error on http error', () =>
      testError(() => taskService.getTasksWithCount(), apiServiceSpy.httpGet));
  });

  describe('#getTaskSettings', () => {

    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(taskSettings)));

    it('should call httpGet with url of /tasks/42/tasksettings', () =>
      testHttpGet(() => taskService.getTaskSettings(42, options),
        apiServiceSpy.httpGet,
        '/tasks/42/tasksettings',
        options,
      ));

    it('should return observable of taskSettings', () => {
      taskService.getTaskSettings(42).subscribe(res =>
        expect(res).toEqual(taskSettings), fail);
    });

    it('should return error on http error', () =>
      testError(() => taskService.getTaskSettings(42), apiServiceSpy.httpGet));
  });

  describe('#getTaskSettingsWithCount', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(taskSettings)));

    it('should call httpGet with url of /tasks/42/tasksettings', () =>
      testHttpGet(() => taskService.getTaskSettingsWithCount(42, options),
        apiServiceSpy.httpGet,
        '/tasks/42/tasksettings',
        options,
        true));

    it('should return observable of taskSettings and count', () =>
      taskService.getTaskSettingsWithCount(42).subscribe(res =>
        expect(res).toEqual({data: taskSettings, count: taskSettings.length}), fail));

    it('should return error on http error', () =>
      testError(() => taskService.getTaskSettings(42), apiServiceSpy.httpGet));
  });

});

