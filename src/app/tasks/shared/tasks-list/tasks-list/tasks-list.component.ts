import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { TaskService } from '../../../task.service';
import { CategoryService } from '../../../../categories/category.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ChildrenOptions, TaskOptions } from '../../../../shared/models/collection-options';
import { updateBehaviorSubject } from '../../../../shared/services/service-utilities';
import { debounceTime, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';
import { Task } from '../../../task.model';
import { TaskSetting } from '../../../tasksetting.model';
import { CategoryItem } from '../../../../categories/category-item';

/**
 * Searchable list of children, can be also filtered by category
 * Fetches task data from TaskService
 */
@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit, OnDestroy {
  // emit when "Add Task" button was clicked
  @Output() addTaskClick = new EventEmitter<Task>();
  // emit when "Add Task Setting" button was clicked
  @Output() addTaskSettingClick = new EventEmitter<TaskSetting>();

  // state of filtering, pagination, etc...
  options: BehaviorSubject<TaskOptions>;
  updateOptions: (options: TaskOptions) => void;
  private tasksSubscription: Subscription;
  private categoriesSubscription: Subscription;
  pageSize: number;
  tasks: Task[];
  isLoading: boolean;
  taskCount: number;
  categoryItems: CategoryItem[] = [];
  taskSettings: { [id: number]: TaskSetting[]; } = {};
  @Input() listTitle: string;
  @Input() paginate: boolean;

  /**
   * @param taskService: used to get list of tasks
   * @param categoryService: used to get list of categories
   */
  constructor(private taskService: TaskService,
              private categoryService: CategoryService) {
  }

  /**
   * On init fetch as many tasks as default page size, fetch all categories
   */
  ngOnInit() {
    // Create subject options to store filtering state
    const defaultOptions = this.paginate ? {limit: this.pageSize, offset: 0} : {};
    this.options = new BehaviorSubject<TaskOptions>(defaultOptions);
    this.updateOptions = updateBehaviorSubject(this.options);
    // subscribe to options to refetch every time options change
    this.tasksSubscription = this.options.pipe(
      debounceTime(environment.DEBOUNCE),
      distinctUntilChanged(),
      tap(() => this.isLoading = true),
      switchMap((options: ChildrenOptions) => this.taskService.getTasksWithCount(options)),
      tap((response: { data: Task[], count: number }) => this.taskCount = response.count),
      map(response => response.data)
    ).subscribe(
      tasks => {
        this.tasks = tasks;
        this.isLoading = false;
      });
    // fetch all categories
    this.categoriesSubscription = this.categoryService.getCategories()
      .subscribe(categories => {
          // map categories to category chip/select items
          this.categoryItems = categories.map(c => new CategoryItem(c));
        }
      );
  }

  /**
   * Unsubscribe from any active subscriptions
   */
  ngOnDestroy() {
    this.categoriesSubscription.unsubscribe();
    this.tasksSubscription.unsubscribe();
  }

  /**
   * Filter categories by ids
   * @param categoryIds: match these ids
   * @returns only those chip items matching ids in parameter
   */
  filterCategories(categoryIds: { categoryId: number }[]): CategoryItem[] {
    return this.categoryItems.filter(c =>
      categoryIds.map(cid => cid.categoryId).includes(c.id));
  }

  /**
   * Called when user selects category, this updates options
   * @param categoryId: selected category, if undefined, options are updated with no category selected
   */
  selectCategory(categoryId: number) {
    this.updateOptions({categoryId});
  }

  /**
   * Called when user clicks on "Add" button next to task
   * @param element: task user clicked on
   */
  onAddTaskClick(element: Task) {
    this.addTaskClick.emit(element);
  }

  /**
   * Called when user clicks on "Add" button next to taskSetting
   * @param element: taskSetting user clicked on
   */
  onAddTaskSettingClick(element: TaskSetting) {
    this.addTaskSettingClick.emit(element);
  }

  /**
   * if task is opened, fetch taskSettings belonging to this task and store them in taskSettings map
   * @param taskId: task that was opened
   */
  openedTask(taskId: number) {
    if (!this.taskSettings[taskId]) {
      this.taskService.getTaskSettings(taskId).subscribe(
        taskSettings => this.taskSettings[taskId] = taskSettings
      );
    }
  }

  /**
   * Filter taskSettings of given task (search filter)
   * Task filtering is done on the server, however taskSettings are fetched
   * separately for better performance. TaskSettings could be also fetched
   * again with search parameter applied, however that would be inefficient
   * given that all tasksettings of given task are probably already cached.
   * Therefore they need to be manually filtered afterwards to use cached
   * version of them.
   * @param task: filter tasksettings of this task
   */
  filteredTaskSettings(task: Task): TaskSetting[] {
    if (!task || !this.taskSettings[task.taskId]) {
      return undefined;
    }

    let searchString = this.options.getValue().search;
    searchString = searchString ? searchString.toLowerCase() : undefined;
    if (!searchString || task.name.toLowerCase().includes(searchString)) {
      return this.taskSettings[task.taskId];
    }
    return this.taskSettings[task.taskId].filter(
      t => t.name.toLowerCase().includes(searchString));
  }
}
