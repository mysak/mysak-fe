import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScenarioDetailPageComponent } from './scenario-detail-page.component';

describe('ScenarioDetailPageComponent', () => {
  let component: ScenarioDetailPageComponent;
  let fixture: ComponentFixture<ScenarioDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScenarioDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScenarioDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
