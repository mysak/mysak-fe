import { Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { ScenarioService } from '../scenario.service';
import { MAX_NAME_LEN, Scenario, ScenarioUpdate } from '../scenario.model';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { Observable, of, Subscription } from 'rxjs';
import { Task } from '../../tasks/task.model';
import { TaskSetting } from '../../tasks/tasksetting.model';
import { TaskOrSettingQueueItem, TaskQueueItem, TaskSettingQueueItem } from '../scenario-queue-item.model';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ConfirmDialogComponent } from '../../shared/widgets/dialogs/confirm-dialog/confirm-dialog.component';
import { InputDialogComponent } from '../../shared/widgets/dialogs/input-dialog/input-dialog.component';
import { environment } from '../../../environments/environment';
import { ComponentCanDeactivate } from '../../shared/guards/unsaved-changes-can-deactivate.guard';
import { QueueItem } from '../../shared/widgets/item-queue/queue-item.model';
import { ErrorDialogComponent } from '../../shared/widgets/dialogs/error-dialog/error-dialog.component';

/**
 * Page that contains detail of scenario, list of tasks and editable queue of tasks
 */
@Component({
  selector: 'app-scenario-detail-page',
  templateUrl: './scenario-detail-page.component.html',
  styleUrls: ['./scenario-detail-page.component.scss']
})
export class ScenarioDetailPageComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
  paginate = environment.PAGINATE;

  @Input() new: boolean;
  scenario$: Observable<Scenario>;
  scenarioId: number;
  queueLoading = true;
  scenarioQueueItems: TaskOrSettingQueueItem[] = [];
  saving = false;
  subscriptions = new Subscription();
  private scenarioLastChange: string;
  dirty = false;

  /**
   * @param route: used to get scenario ID from route
   * @param router: used to navigate back when successfully edited
   * @param scenariosService: used to fetch scenario detail from API
   * @param snackBar: used to display message when successful
   * @param dialog: used to display confirmation dialog
   */
  constructor(private route: ActivatedRoute,
              private router: Router,
              private scenariosService: ScenarioService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  /**
   * Router guard, returns true if it is safe to deactivate route
   * @returns true if state is not dirty
   */
  @HostListener('window:beforeunload', ['$event'])
  canDeactivate(): boolean {
    return !this.dirty;
  }

  /**
   * Called when Queue items change (user added or removed item or changed their order)
   * Mark state as dirty
   * @param items: new content of queue
   */
  onItemsChange(items: QueueItem[]) {
    this.dirty = true;
    this.scenarioQueueItems = items as TaskOrSettingQueueItem[];
  }

  /**
   * Get scenario ID from route and fetch scenario detail from API
   * If ID is 'new', it means that new scenario is being created instead of editing an existing one,
   * in that case, boolean flag new is set to true
   */
  ngOnInit() {
    this.scenario$ = this.route.paramMap.pipe(
      map(pMap => pMap.get('id')),
      tap(id => this.new = id === 'new'),
      switchMap(sid => sid === 'new' ?
        of({name: '', note: ''} as Scenario) :
        this.scenariosService.getScenario(+sid).pipe(
          tap(scenario => {
            // Remember scenario Id
            this.scenarioId = scenario.scenarioId;
            // Remember scenario last Change
            this.scenarioLastChange = scenario.lastChange;
            // Map queue data to queue items based on their type
            this.scenarioQueueItems = scenario.items.map(i =>
              i.type === 'task' ?
                new TaskQueueItem(i.data as Task) :
                new TaskSettingQueueItem(i.data as TaskSetting)
            );
          })))).pipe(tap(() => this.queueLoading = false));
  }

  /**
   * Unsubscribe from any active subscriptions
   */
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  /**
   * Disable saving flag and if successful, display snackbar message
   * @param res: updated scenario
   */
  _saveMessage = res => {
    this.saving = false;
    if (res && res.scenarioId && res.name) {
      this.snackBar.open('Scénář ' + res.name + ' uložen',
        undefined, {duration: environment.SNACKBAR_DURATION});
    }
  };

  /**
   * Disables saving flag and logs error to console
   * @param e: error
   */
  _handleError = e => {
    this.saving = false;
    console.error('ERROR', e);
  };

  /**
   * Called when user clicks on "Save as" button
   * Displays confirm dialog an then saves the scenario
   * @param name: new scenario name
   * @param note: new scenario note
   */
  saveAs(name: string, note: string) {
    const newScenario = new ScenarioUpdate(
      name, note, this.scenarioQueueItems.map(i => i.getSaveItem()));
    this.subscriptions.add(this.dialog.open(InputDialogComponent, {
      data: {
        title: 'Opravdu chcete uložit změny?',
        input: {
          hint: 'Zadejte nový název',
          initial: name,
          sameInputAllowed: false,
          maxLength: MAX_NAME_LEN,
          sameInputError: 'Název se musí lišit',
          emptyInputError: 'Název nesmí být prázdný',
          placeholder: 'Název',
        }
      }
    }).afterClosed().pipe(
      // check if confirmed
      filter(confirmed => confirmed),
      tap(() => this.saving = true),
      switchMap((res: string) => {
        newScenario.name = res;
        // create new scenario
        return this.scenariosService.createScenario(newScenario);
      }),
    ).subscribe(this._saveMessage, this._handleError));
  }

  /**
   * Called when user clicks on "Save" button
   * Displays confirm dialog an then saves the scenario
   * If new flag is true, saves new scenario, otherwise updates an existing one
   * @param name: new scenario name
   * @param note: new scenario note
   */
  save(name: string, note: string) {
    // check if name is empty
    if (name.length === 0) {
      this.dialog.open(ErrorDialogComponent, {data: {title: 'Doplňte název', content: 'Název nesmí být prázdný'}});
      return;
    }
    const newScenario = new ScenarioUpdate(
      name, note, this.scenarioQueueItems.map(i => i.getSaveItem()));

    // check if new scenario is created or existing being updated, for the latter, display confirm dialog
    const dialogResult$ = this.new ?
      of(true) :
      this.dialog.open(ConfirmDialogComponent,
        {data: {content: name, title: 'Opravdu chcete uložit změny?'}}).afterClosed().pipe(
        filter(confirmed => confirmed));

    // subscribe to result of previous dialog and create or update scenario based on 'new' flag
    this.subscriptions.add(
      dialogResult$.pipe(
        tap(() => this.saving = true),
        switchMap(() => this.new ?
          this.scenariosService.createScenario(newScenario) :
          this.scenariosService.updateScenario(this.scenarioId, this.scenarioLastChange, newScenario))
      ).subscribe(res => {
        this.dirty = false;
        this._saveMessage(res);
        return this.router.navigate(['../'], {relativeTo: this.route});
      }, this._handleError));
  }

  /**
   * Called when user clicks on "Add" button next to task
   * @param task: to be added to the queue
   */
  onAddTaskClick(task: Task) {
    this.dirty = true;
    this.scenarioQueueItems.push(new TaskQueueItem(task));
  }

  /**
   * Called when user clicks on "Add" button next to specific taskSetting
   * @param taskSetting: to be added to the queue
   */
  onAddTaskSettingClick(taskSetting: TaskSetting) {
    this.dirty = true;
    this.scenarioQueueItems.push(new TaskSettingQueueItem(taskSetting));
  }

  /**
   * If detail has changed (name or note), mark state as dirty
   */
  onDetailChange() {
    this.dirty = true;
  }
}
