import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScenarioDetailEditComponent } from './scenario-detail-edit/scenario-detail-edit.component';
import { SharedMaterialModule } from '../../../shared/shared-material/shared-material.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ScenarioDetailEditComponent],
  imports: [
    CommonModule,
    SharedMaterialModule,
    FormsModule
  ],
  exports: [
    ScenarioDetailEditComponent
  ]
})
export class ScenarioDetailEditModule { }
