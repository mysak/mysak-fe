import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Scenario } from '../../../scenario.model';
import { ScenarioService } from '../../../scenario.service';
import { debounceTime, distinctUntilChanged, filter, map, switchMap, tap } from 'rxjs/operators';
import { MatDialog, MatSnackBar, Sort } from '@angular/material';
import { environment } from '../../../../../environments/environment';
import { updateBehaviorSubject } from '../../../../shared/services/service-utilities';
import { ScenariosOptions } from '../../../../shared/models/collection-options';
import { ConfirmDialogComponent } from '../../../../shared/widgets/dialogs/confirm-dialog/confirm-dialog.component';
import { EduappService } from '../../../../eduapps/eduapp.service';
import { Router } from '@angular/router';

/**
 * Sortable, searchable list of scenarios
 * Fetches scenarios data from ScenarioService
 */
@Component({
  selector: 'app-scenarios-list',
  templateUrl: './scenarios-list.component.html',
  styleUrls: ['./scenarios-list.component.scss']
})
export class ScenariosListComponent implements OnInit {
  eduappId: number;

  scenarios$: Observable<Scenario[]>;
  scenariosCount: number;
  deleting = [];
  // Columns that are displayed
  @Input() displayedColumns: String[];
  // Buttons that are displayed, allowed values: 'assign', 'delete', 'edit'
  @Input() actionButtons: ('assign' | 'delete' | 'edit')[];
  // Emit when user clicks on "Add" button next to scenario
  @Input() listTitle: string;
  // Whether the list should be paginated
  @Input() paginate: boolean;

  @Output() addClick = new EventEmitter<Scenario>();

  // State of sorting, pagination, etc...
  options: BehaviorSubject<ScenariosOptions>;
  pageSize: number;
  loading = false;

  updateOptions: (options: ScenariosOptions) => void;

  /**
   * @param scenariosService: used to get list of children
   * @param router: used to navigate to scenario detail
   * @param eduappService: used to get selected eduapp ID for edit button redirect
   * @param snackBar: used to display message on successful scenario deletion
   * @param dialog: used to display confirmation dialog
   */
  constructor(private scenariosService: ScenarioService,
              private router: Router,
              private eduappService: EduappService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  /**
   * On init fetch as many scenarios as default page size
   */
  ngOnInit() {
    // Get eduapp ID from route
    this.eduappId = this.eduappService.getSelectedId();
    this.pageSize = environment.DEFAULT_PAGE_SIZE;
    // Create subject options to store filtering state
    const defaultOptions = this.paginate ? {limit: this.pageSize, offset: 0} : {};
    this.options = new BehaviorSubject<ScenariosOptions>(defaultOptions);
    this.updateOptions = updateBehaviorSubject(this.options);

    // subscribe to options to refetch every time options change
    this.scenarios$ = this.options.pipe(
      debounceTime(environment.DEBOUNCE),
      distinctUntilChanged(),
      tap(() => this.loading = true),
      switchMap((options: ScenariosOptions) => this.scenariosService.getScenariosWithCount(options)),
      tap((response: { data: Scenario[], count: number }) => this.scenariosCount = response.count),
      map(response => response.data),
      tap(() => this.loading = false)
    );
  }

  /**
   * Called when user clicks on "Add" button next to scenario
   * @param element: scenario user clicked on
   */
  onAddClick(element: Scenario): void {
    this.addClick.emit(element);
  }

  /**
   * Called when user click on sort by column
   * @param sort: options containing sorted column and direction
   * Updates options with new sort query params
   */
  onSort(sort: Sort): void {
    this.updateOptions({
      sort:
        (sort.direction === 'desc' ? '-' : '') + sort.active.toString()
    });
  }

  onDeleteClick(scenario: Scenario) {
    this.dialog.open(ConfirmDialogComponent, {
      data: {title: 'Opravdu chcete smazat tento scénář?', content: scenario.name}
    }).afterClosed().pipe(
      filter(res => res),
      tap(() => this.deleting[scenario.scenarioId] = true),
      switchMap(e => this.scenariosService.deleteScenario(scenario.scenarioId)),
      tap(() => this.options.next(Object.assign({}, this.options.getValue()))),
    ).subscribe(
      () => {
        this.snackBar.open('Scénář ' + scenario.name + ' byl úspěšně smazán',
          undefined,
          {duration: environment.SNACKBAR_DURATION});
        delete this.deleting[scenario.scenarioId];
      },
      err => {
        console.error(err);
        delete this.deleting[scenario.scenarioId];
      });
  }
  onScenarioClick(s: Scenario) {
    this.router.navigateByUrl('/eduapps/' + this.eduappId + '/scenarios/' + s.scenarioId);
  }
}
