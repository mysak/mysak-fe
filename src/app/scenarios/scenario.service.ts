import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { MAX_NAME_LEN, Scenario, ScenarioUpdate } from './scenario.model';
import { processCountHeader, toParams } from '../shared/services/service-utilities';
import { ApiService } from '../shared/services/api.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ScenariosOptions } from '../shared/models/collection-options';
import { DialogErrorStrategy, NoMessageErrorStrategy } from '../shared/services/error-strategy';
import { InputDialogComponent } from '../shared/widgets/dialogs/input-dialog/input-dialog.component';
import { MatDialog } from '@angular/material';

/**
 * When lastChange freshly fetched from server doesn't match the one cached, this error is raised
 */
class AlreadyChangedError implements Error {
  message = 'Someone else has changed the scenario';
  name = 'Already changed error';
}


/**
 * Fetch and alter scenarios from API using ApiService
 */
@Injectable({
  providedIn: 'root'
})
export class ScenarioService {

  private scenarioUrlSuffix = '/scenarios';

  /**
   * Requires ApiService to fetch from API
   * Requires MatDialog to display input dialog
   * @param apiService: uses this service to fetch from API
   * @param dialog: uses MatDialog to ask for input on AlreadyChangedError
   */
  constructor(private apiService: ApiService,
              private dialog: MatDialog) {
  }

  /**
   * Get scenarios matching selected options, if no options provided, get all scenarios
   * Fetches /scenarios?{options}
   * @param options: allows to specify which scenarios to retrieve, options are serialized into query params
   * @returns observable of fetched scenarios matching criteria
   */
  getScenarios(options?: ScenariosOptions): Observable<Scenario[]> {
    return this.getScenariosWithCount(options).pipe(map(response => response.data));
  }

  /**
   * Get scenarios matching selected options, if no options provided, get all children
   * Fetches /scenarios?{options}
   * @param options: allows to specify which scenarios to retrieve, options are serialized into query params
   * @returns observable of fetched categories matching criteria with total count of categories matching criteria
   * (with no respect to pagination)
   */
  getScenariosWithCount(options?: ScenariosOptions): Observable<{ data: Scenario[], count: number }> {
    return processCountHeader(this.apiService.httpGet<Scenario[]>(
      this.scenarioUrlSuffix,
      {params: toParams(options), observe: 'response'}
    ));
  }

  /**
   * Get scenario with specific ID
   * Fetches /scenarios/{id} from API
   * @param id: scenario to fetch
   * @returns observable of fetched scenario matching id
   */
  getScenario(id: number): Observable<Scenario> {
    return this.apiService.httpGet<Scenario>(
      this.scenarioUrlSuffix + '/' + id,
    );
  }

  /**
   * Update scenario with id given by parameter
   * If someone has already edited the scenario while the user attempts to save it, the user is warned
   * and prompted to save it under a different name.
   * To check for this type of error, the method first needs to fetch lastChange from API and compare with the one given by parameter
   * If they don't match, previously mentioned error occurs
   * Sends GET and PUT requests to /scenarios/{id}
   * @param id: scenario to update
   * @param lastChange: when was scenario last changed (to check if someone has already edited it)
   * @param update: new Tasks or TaskSettings to set to the scenario
   * @returns observable of updated scenario
   */
  updateScenario(id: number, lastChange: string, update: ScenarioUpdate): Observable<Scenario> {
    // error handler function specific to updating scenario
    const handleError = (e) => {
      if (e instanceof AlreadyChangedError) {
        // Open dialog to prompt for new name
        return this.dialog.open(InputDialogComponent, {
          data: {
            title: 'Chyba',
            content: 'Scénář byl mezitím někým upraven, prosím zadejte nový název',
            error: true,
            input: {
              hint: 'Nový název',
              initial: update.name,
              sameInputAllowed: false,
              sameInputError: 'Název se musí lišit',
              emptyInputError: 'Název je povinný',
              maxLength: MAX_NAME_LEN,
            }
          }
        }).afterClosed().pipe(
          switchMap((name: string) => {
            update.name = name;
            // save scenario under new name
            return name && name.length ? this.createScenario(update) : throwError(e);
          }));
      } else {
        // Other types of errors: just notify the user
        return new DialogErrorStrategy(this.dialog).handleError(e, 'Scénář se nepodařilo uložit');
      }
    };
    // First fetch from the API to check for lastChange
    return this.getScenario(id).pipe(
      switchMap(scenario => scenario && (scenario.lastChange !== lastChange) ?
        // cached lastChange and the one received from server don't match
        throwError(new AlreadyChangedError()) :
        // the scenario is safe to save without overwriting someone else's changes
        this.apiService.httpPut<Scenario>(
          this.scenarioUrlSuffix + '/' + id,
          update,
          {},
          {message: 'abc', strategy: new NoMessageErrorStrategy()})),
      catchError(handleError));
  }

  /**
   * Creates scenario given by parameter
   * Sends POST request to /scenarios
   * @param create: scenario to create
   * @returns observable of created scenario
   */
  createScenario(create: ScenarioUpdate): Observable<Scenario> {
    return this.apiService.httpPost<Scenario>(
      this.scenarioUrlSuffix,
      create,
      {},
      {message: 'Nepodařilo se vytvořit scénář'}
    );
  }

  /**
   * Deletes scenario given by parameter
   * Sends DELETE request to /scenarios/{id}
   * @param id: scenario to delete
   */
  deleteScenario(id: number) {
    return this.apiService.httpDelete(
      this.scenarioUrlSuffix + '/' + id,
      {},
      {message: 'Nepodařilo se smazat scénář'}
    );
  }
}
