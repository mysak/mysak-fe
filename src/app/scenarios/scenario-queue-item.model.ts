import { QueueItem } from '../shared/widgets/item-queue/queue-item.model';
import { TaskSetting } from '../tasks/tasksetting.model';
import { Task } from '../tasks/task.model';
import { Scenario, ScenarioSaveItem } from './scenario.model';

/**
 * Queue Item interface implementation with constructor
 */
abstract class QueueItemImpl implements QueueItem {
  /**
   * Constructs QueueItemImpl from queue item object
   * @param item: queue item object
   */
  protected constructor(item: QueueItem) {
    this.id = item.id;
    this.image = item.image;
    this.label = item.label;
    this.item = item.item;
    this.secondary = item.secondary;
    this.dragDisabled = item.dragDisabled;
    this.removeDisabled = item.removeDisabled;
  }

  dragDisabled: boolean;
  id: number;
  image: string;
  item: any;
  label: string;
  secondary?: string;
  removeDisabled: boolean;
}

/**
 * Task or TaskSetting queue item abstract class
 */
export abstract class TaskOrSettingQueueItem extends QueueItemImpl {
  protected constructor(item: QueueItem) {
    super(item);
  }

  /**
   * abstract method to get API requested representation of item
   */
  abstract getSaveItem(): ScenarioSaveItem;
}

/**
 * Queue item containing a scenario, used i.e. in child detail page queue
 */
export class ScenarioQueueItem extends QueueItemImpl {
  constructor(s: Scenario) {
    super({
      id: s.scenarioId,
      image: s.image ? s.image.resourceUri : undefined,
      label: s.name,
      item: s,
      secondary: s.note
    });
  }
}

export class TaskQueueItem extends TaskOrSettingQueueItem {
  /**
   * Constructs queue item from task
   * @param t: task to transform to queue item
   */
  constructor(t: Task) {
    super({
      id: t.taskId,
      image: t.image ? t.image.resourceUri : undefined,
      label: t.name,
      item: t,
      secondary: 'Vybrat automaticky',
    });
  }

  /**
   * Get API requested representation of item
   */
  getSaveItem(): ScenarioSaveItem {
    return {taskId: this.item.taskId, type: 'task'};
  }
}

export class TaskSettingQueueItem extends TaskOrSettingQueueItem {
  /**
   * Constructs queue item from taskSetting
   * @param t: taskSetting to transform to queue item
   */
  constructor(t: TaskSetting) {
    super({
      id: t.taskSettingId,
      image: t.image ? t.image.resourceUri : undefined,
      label: t.name,
      item: t,
      secondary: 'Obtížnost: ' + t.difficulty,
    });
  }

  /**
   * Get API requested representation of item
   */
  getSaveItem(): ScenarioSaveItem {
    return {taskId: this.item.taskId, taskSettingId: this.item.taskSettingId, type: 'taskSetting'};
  }
}
