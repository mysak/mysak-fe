import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScenariosRoutingModule } from './scenarios-routing.module';
import { ScenarioDetailPageComponent } from './scenario-detail-page/scenario-detail-page.component';
import { ScenariosPageComponent } from './scenarios-page/scenarios-page.component';
import { SharedMaterialModule } from '../shared/shared-material/shared-material.module';
import { ScenariosListModule } from './shared/scenarios-list/scenarios-list.module';
import { ScenarioAssignPageComponent } from './scenario-assign-page/scenario-assign-page.component';
import { ChildrenListModule } from '../children/shared/children-list/children-list.module';
import { TasksListModule } from '../tasks/shared/tasks-list/tasks-list.module';
import { ItemQueueModule } from '../shared/widgets/item-queue/item-queue.module';
import { ScenarioDetailEditModule } from './shared/scenario-detail-edit/scenario-detail-edit.module';
import { ToolbarTitleModule } from '../shared/widgets/toolbar-title/toolbar-title.module';

@NgModule({
  declarations: [ScenarioDetailPageComponent, ScenariosPageComponent, ScenarioAssignPageComponent],
  imports: [
    CommonModule,
    SharedMaterialModule,
    ScenariosRoutingModule,
    ScenariosListModule,
    ChildrenListModule,
    TasksListModule,
    ItemQueueModule,
    ScenarioDetailEditModule,
    ToolbarTitleModule,
  ]
})
export class ScenariosModule {}
