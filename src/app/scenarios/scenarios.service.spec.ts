import { TestBed } from '@angular/core/testing';

import { of, throwError } from 'rxjs';
import { ScenariosOptions } from '../shared/models/collection-options';
import { TEST_SCENARIOS } from '../shared/testing/data/Scenarios';
import { ScenarioService } from './scenario.service';
import { ApiService } from '../shared/services/api.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { InputDialogComponent } from '../shared/widgets/dialogs/input-dialog/input-dialog.component';
import {
  observeResonse,
  testError,
  testHttpDelete,
  testHttpGet,
  testHttpPost,
  testHttpPut
} from '../shared/testing/utility/service-utilities';
import { ScenarioUpdate } from './scenario.model';
import { HttpErrorResponse } from '@angular/common/http';

describe('ScenarioService', () => {
  let scenarioService: ScenarioService;
  let apiServiceSpy: jasmine.SpyObj<ApiService>;
  let dialogSpy: jasmine.SpyObj<MatDialog>;
  let openedDialogSpy: jasmine.SpyObj<MatDialogRef<InputDialogComponent>>;
  const scenario$ = of(TEST_SCENARIOS[0]);
  const scenario = TEST_SCENARIOS[0];
  const scenarios$ = of(TEST_SCENARIOS);
  const scenarios = TEST_SCENARIOS;
  const options: ScenariosOptions = {
    limit: 100,
    offset: 50,
    search: 'abc',
    sort: 'name',
  };

  beforeEach(() => {
    const spy = jasmine.createSpyObj('ApiService', ['httpGet', 'httpPost', 'httpPut', 'httpPatch', 'httpDelete']);
    const dialogSpyDep = jasmine.createSpyObj('MatDialog', ['open']);
    TestBed.configureTestingModule(
      {
        providers: [
          ScenarioService,
          {provide: ApiService, useValue: spy},
          {provide: MatDialog, useValue: dialogSpyDep},
        ]
      });
    scenarioService = TestBed.get(ScenarioService);
    apiServiceSpy = TestBed.get(ApiService);
    dialogSpy = TestBed.get(MatDialog);
    openedDialogSpy = jasmine.createSpyObj('MatDialogRef<InputDialogComponent>', ['afterClosed']);
  });

  it('should be created', () => {
    expect(scenarioService).toBeTruthy();
    expect(apiServiceSpy).toBeTruthy();
    expect(openedDialogSpy).toBeTruthy();
  });

  describe('#getScenario', () => {

    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(scenario$));

    it('should call httpGet with id of scenario', () =>
      testHttpGet(() => scenarioService.getScenario(42), apiServiceSpy.httpGet, '/scenarios/42'));

    it('should return observable of scenario', () =>
      scenarioService.getScenario(1)
        .subscribe(res => expect(res).toEqual(scenario), fail));

    it('should return error on http error', () =>
      testError(() => scenarioService.getScenario(42), apiServiceSpy.httpGet));

  });

  describe('#getScenarios', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(scenarios)));

    it('should call httpGet with url of /scenarios', () =>
      testHttpGet(() => scenarioService.getScenarios(options),
        apiServiceSpy.httpGet,
        '/scenarios',
        options
      ));

    it('should return observable of scenarios', () => {
      scenarioService.getScenarios().subscribe(res =>
        expect(res).toEqual(scenarios), fail);
    });

    it('should return error on http error', () =>
      testError(() => scenarioService.getScenarios(), apiServiceSpy.httpGet));

  });
  describe('#getScenariosWithCount', () => {
    beforeEach(() => apiServiceSpy.httpGet.and.returnValue(observeResonse(scenarios)));

    it('should call httpGet with url of /scenarios', () =>
      testHttpGet(() => scenarioService.getScenariosWithCount(options),
        apiServiceSpy.httpGet,
        '/scenarios',
        options,
        true));

    it('should return observable of scenarios and count', () =>
      scenarioService.getScenariosWithCount().subscribe(res =>
        expect(res).toEqual({data: scenarios, count: scenarios.length}), fail));

    it('should return error on http error', () =>
      testError(() => scenarioService.getScenarios(), apiServiceSpy.httpGet));
  });

  describe('#createScenario', () => {
    const create = new ScenarioUpdate('testScenario', 'testNote', [
      {taskId: 1, taskSettingId: 2, type: 'taskSetting'},
      {taskId: 2, type: 'task'},
      {taskId: 3, type: 'task'},
    ]);

    beforeEach(() => apiServiceSpy.httpPost.and.returnValue(of(scenario)));

    it('should call httpPost with url of /scenarios', () =>
      testHttpPost(() => scenarioService.createScenario(create),
        apiServiceSpy.httpPost,
        '/scenarios',
        create
      ));

    it('should return observable of scenario', () =>
      scenarioService.createScenario(create).subscribe(res =>
        expect(res).toEqual(scenario), fail));

    it('should return error on http error', () =>
      testError(() => scenarioService.createScenario(create),
        apiServiceSpy.httpPost));
  });

  describe('#deleteScenario', () => {

    beforeEach(() => apiServiceSpy.httpDelete.and.returnValue(of()));

    it('should call httpDelete with url of /scenarios/{id}', () =>
      testHttpDelete(() => scenarioService.deleteScenario(42),
        apiServiceSpy.httpDelete,
        '/scenarios/42'));

    it('should return error on http error', () =>
      testError(() => scenarioService.deleteScenario(42), apiServiceSpy.httpDelete));

  });


  describe('#updateScenario', () => {
    const update = new ScenarioUpdate('testScenario', 'testNote', [
      {taskId: 1, taskSettingId: 2, type: 'taskSetting'},
      {taskId: 2, type: 'task'},
      {taskId: 3, type: 'task'},
    ]);

    beforeEach(() => {
      apiServiceSpy.httpPut.and.returnValue(of(scenario));
      apiServiceSpy.httpPost.and.returnValue(of(scenario));
      apiServiceSpy.httpGet.and.returnValue(of(scenario));
    });

    it('should call httpGet and httpPut with url of /scenarios/{id}', () => {
      testHttpGet(() => scenarioService.updateScenario(42, scenario.lastChange, update),
        apiServiceSpy.httpGet,
        '/scenarios/42',
      );
      testHttpPut(() => of(),
        apiServiceSpy.httpPut,
        '/scenarios/42',
        update
      );
    });

    describe('#updateScenario already changed error', () => {
      beforeEach(() => {
        openedDialogSpy.afterClosed.and.returnValue(of('newName'));
        dialogSpy.open.and.returnValue(openedDialogSpy);
      });

      afterEach(() => {
        expect(dialogSpy.open).toHaveBeenCalledTimes(1);
        expect(dialogSpy.open.calls.mostRecent().args[0]).toBe(InputDialogComponent);
        expect(openedDialogSpy.afterClosed).toHaveBeenCalledTimes(1);
      });

      it('should show input error dialog on lastChange mismatch', () => {
        scenarioService.updateScenario(42, 'SHOULD-THROW-ERROR', update)
          .subscribe(res => res, fail);


        testHttpPost(() => of(),
          apiServiceSpy.httpPost,
          '/scenarios',
          new ScenarioUpdate('newName', update.note, update.tasks));
      });

      it('should throw error when httpPost fails', () => {
        apiServiceSpy.httpPost.and.returnValue(throwError(new HttpErrorResponse({error: 'testError'})));
        scenarioService.updateScenario(42, 'SHOULD-THROW-ERROR', update)
          .subscribe(() => fail('should have failed'),
            e => expect(e.error).toEqual('testError'));
      });

      it('should return observable of scenario', () =>
        scenarioService.updateScenario(42, 'SHOULD-THROW-ERROR', update).subscribe(res =>
          expect(res).toEqual(scenario), fail));
    });

    it('should return observable of scenario', () =>
      scenarioService.updateScenario(42, scenario.lastChange, update).subscribe(res =>
        expect(res).toEqual(scenario), fail));

    it('should return error on http error', () =>
      testError(() => scenarioService.updateScenario(42, scenario.lastChange, update),
        apiServiceSpy.httpPut));
  });
});
