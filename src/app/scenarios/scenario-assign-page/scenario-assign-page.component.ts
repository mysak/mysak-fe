import { Component, OnInit } from '@angular/core';
import { Child } from '../../children/child.model';
import { AssignmentType, ChildService } from '../../children/child.service';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ScenarioService } from '../scenario.service';
import { Observable } from 'rxjs';
import { Scenario } from '../scenario.model';
import { SelectionChange } from '@angular/cdk/collections';
import { environment } from '../../../environments/environment';
import { switchMap, tap } from 'rxjs/operators';

/**
 * Page that contains selectable list of children and assign buttons
 */
@Component({
  selector: 'app-scenario-assign-page',
  templateUrl: './scenario-assign-page.component.html',
  styleUrls: ['./scenario-assign-page.component.scss']
})
export class ScenarioAssignPageComponent implements OnInit {
  paginate = environment.PAGINATE;

  scenario$: Observable<Scenario>;
  scenarioId: number;
  saving = false;
  selected: Child[] = [];

  /**
   * @param childService: used to fetch children from API
   * @param scenariosService: used to fetch scenario detail from API
   * @param snackBar: used to display message when succeeded
   * @param route: used to get scenario id from route
   */
  constructor(private childService: ChildService,
              private scenariosService: ScenarioService,
              public snackBar: MatSnackBar,
              private route: ActivatedRoute) {
  }

  /**
   * Get scenario ID from route and fetch scenario with that ID
   */
  ngOnInit() {
    this.scenario$ = this.route.paramMap.pipe(
      switchMap(pMap => this.scenariosService.getScenario(+pMap.get('id'))),
      tap(scenario => this.scenarioId = scenario.scenarioId));
  }

  /**
   * Called when child is selected or unselected
   * @param selection: changes in selection
   */
  onSelectionChange(selection: SelectionChange<Child>): void {
    this.selected = selection.source.selected;
  }

  /**
   * Called when Assign to Front button is pressed, assigns the scenario to front of children's queue
   */
  assignFront = (): void => this._assign(AssignmentType.FRONT);
  /**
   * Called when Assign to Back button is pressed, assigns the scenario to back of children's queue
   */
  assignBack = (): void => this._assign(AssignmentType.BACK);

  /**
   * Assign scenario to back or front of the queue based on the parameter
   * Display snackbar message if successful
   * @param type: Front or Back
   */
  _assign(type: AssignmentType): void {
    this.saving = true;
    // assign scenario
    this.childService.assignScenario(
      this.selected.map(ch => ch.childId),
      this.scenarioId,
      type
    ).subscribe(
      () => {
        // display message
        this.snackBar.open('Scénář přiřazen ' + this.selected.length +
          (this.selected.length > 1 ? ' dětem' : ' dítěti'),
          undefined, {duration: environment.SNACKBAR_DURATION});
        this.saving = false;
      }, () => this.saving = false);
  }
}
