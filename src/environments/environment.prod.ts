export const environment = {
  production: true,
  apiBase: 'http://private-1df806-mysak.apiary-mock.com', // TODO: replace with backend url
  DEBOUNCE: 10,
  SNACKBAR_DURATION: 2000,
  MAX_CACHE_AGE_MS: 30000,
  DEFAULT_PAGE_SIZE: 10,
  PAGINATE: false,
  // NEVER CACHE INDIVIDUAL CHILD or SCENARIO WITH CACHE-THEN-REFETCH STRATEGY,
  // may cause user to loose his queue changes
  // By default only GET requests are cacheable, use string or regex
  CACHEABLE: ['/eduapps', '/children', '/scenarios', '/tasks', '/groups', '/categories',
    /\/tasks\/.*\/tasksettings/
  ],
};
