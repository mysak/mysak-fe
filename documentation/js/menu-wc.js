'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">mysak-fe documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="dependencies.html" data-type="chapter-link">
                                <span class="icon ion-ios-list"></span>Dependencies
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-0f1b0860ed2f73b677ce2c6824adbd9a"' : 'data-target="#xs-components-links-module-AppModule-0f1b0860ed2f73b677ce2c6824adbd9a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-0f1b0860ed2f73b677ce2c6824adbd9a"' :
                                            'id="xs-components-links-module-AppModule-0f1b0860ed2f73b677ce2c6824adbd9a"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ChildDetailModule.html" data-type="entity-link">ChildDetailModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ChildDetailModule-1018a30d5b71adfb1eb20ddbd5f07e06"' : 'data-target="#xs-components-links-module-ChildDetailModule-1018a30d5b71adfb1eb20ddbd5f07e06"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ChildDetailModule-1018a30d5b71adfb1eb20ddbd5f07e06"' :
                                            'id="xs-components-links-module-ChildDetailModule-1018a30d5b71adfb1eb20ddbd5f07e06"' }>
                                            <li class="link">
                                                <a href="components/ChildDetailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChildDetailComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChildrenListModule.html" data-type="entity-link">ChildrenListModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ChildrenListModule-080d8b0d1c7b86bdf28fec86da305056"' : 'data-target="#xs-components-links-module-ChildrenListModule-080d8b0d1c7b86bdf28fec86da305056"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ChildrenListModule-080d8b0d1c7b86bdf28fec86da305056"' :
                                            'id="xs-components-links-module-ChildrenListModule-080d8b0d1c7b86bdf28fec86da305056"' }>
                                            <li class="link">
                                                <a href="components/ChildrenListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChildrenListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChildrenModule.html" data-type="entity-link">ChildrenModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ChildrenModule-687dc1629ba4e4caf7220e817160f57a"' : 'data-target="#xs-components-links-module-ChildrenModule-687dc1629ba4e4caf7220e817160f57a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ChildrenModule-687dc1629ba4e4caf7220e817160f57a"' :
                                            'id="xs-components-links-module-ChildrenModule-687dc1629ba4e4caf7220e817160f57a"' }>
                                            <li class="link">
                                                <a href="components/ChildDetailPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChildDetailPageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChildrenPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChildrenPageComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChildrenRoutingModule.html" data-type="entity-link">ChildrenRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DataFilteringModule.html" data-type="entity-link">DataFilteringModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DataFilteringModule-20d99071038af5511653c43211e0af77"' : 'data-target="#xs-components-links-module-DataFilteringModule-20d99071038af5511653c43211e0af77"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DataFilteringModule-20d99071038af5511653c43211e0af77"' :
                                            'id="xs-components-links-module-DataFilteringModule-20d99071038af5511653c43211e0af77"' }>
                                            <li class="link">
                                                <a href="components/DataFilteringComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DataFilteringComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DataTableModule.html" data-type="entity-link">DataTableModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DataTableModule-01c71dd673c6894134fe6123b6eb5761"' : 'data-target="#xs-components-links-module-DataTableModule-01c71dd673c6894134fe6123b6eb5761"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DataTableModule-01c71dd673c6894134fe6123b6eb5761"' :
                                            'id="xs-components-links-module-DataTableModule-01c71dd673c6894134fe6123b6eb5761"' }>
                                            <li class="link">
                                                <a href="components/DataTableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DataTableComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DialogsModule.html" data-type="entity-link">DialogsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DialogsModule-a921a057b889e3e1a72d21d994fdf7d1"' : 'data-target="#xs-components-links-module-DialogsModule-a921a057b889e3e1a72d21d994fdf7d1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DialogsModule-a921a057b889e3e1a72d21d994fdf7d1"' :
                                            'id="xs-components-links-module-DialogsModule-a921a057b889e3e1a72d21d994fdf7d1"' }>
                                            <li class="link">
                                                <a href="components/ConfirmDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ErrorDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ErrorDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InputDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InputDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-DialogsModule-a921a057b889e3e1a72d21d994fdf7d1"' : 'data-target="#xs-directives-links-module-DialogsModule-a921a057b889e3e1a72d21d994fdf7d1"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-DialogsModule-a921a057b889e3e1a72d21d994fdf7d1"' :
                                        'id="xs-directives-links-module-DialogsModule-a921a057b889e3e1a72d21d994fdf7d1"' }>
                                        <li class="link">
                                            <a href="directives/ForbiddenStringValidatorDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">ForbiddenStringValidatorDirective</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/EduAppsModule.html" data-type="entity-link">EduAppsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-EduAppsModule-352cf07434a5675da1b739997d4ace16"' : 'data-target="#xs-components-links-module-EduAppsModule-352cf07434a5675da1b739997d4ace16"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EduAppsModule-352cf07434a5675da1b739997d4ace16"' :
                                            'id="xs-components-links-module-EduAppsModule-352cf07434a5675da1b739997d4ace16"' }>
                                            <li class="link">
                                                <a href="components/EduappDetailPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EduappDetailPageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EduappsPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EduappsPageComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EduappsRoutingModule.html" data-type="entity-link">EduappsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/FakeTestingModule.html" data-type="entity-link">FakeTestingModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-FakeTestingModule-11c3dc7b0455b4d34dce1a6663b8fd04"' : 'data-target="#xs-components-links-module-FakeTestingModule-11c3dc7b0455b4d34dce1a6663b8fd04"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FakeTestingModule-11c3dc7b0455b4d34dce1a6663b8fd04"' :
                                            'id="xs-components-links-module-FakeTestingModule-11c3dc7b0455b4d34dce1a6663b8fd04"' }>
                                            <li class="link">
                                                <a href="components/ItemSelectStubComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ItemSelectStubComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-FakeTestingModule-11c3dc7b0455b4d34dce1a6663b8fd04"' : 'data-target="#xs-directives-links-module-FakeTestingModule-11c3dc7b0455b4d34dce1a6663b8fd04"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-FakeTestingModule-11c3dc7b0455b4d34dce1a6663b8fd04"' :
                                        'id="xs-directives-links-module-FakeTestingModule-11c3dc7b0455b4d34dce1a6663b8fd04"' }>
                                        <li class="link">
                                            <a href="directives/RouterLinkDirectiveStub.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">RouterLinkDirectiveStub</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ItemChipsModule.html" data-type="entity-link">ItemChipsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ItemChipsModule-adeb158f46d4627cc8443f335c3c6852"' : 'data-target="#xs-components-links-module-ItemChipsModule-adeb158f46d4627cc8443f335c3c6852"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ItemChipsModule-adeb158f46d4627cc8443f335c3c6852"' :
                                            'id="xs-components-links-module-ItemChipsModule-adeb158f46d4627cc8443f335c3c6852"' }>
                                            <li class="link">
                                                <a href="components/ItemChipsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ItemChipsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ItemQueueModule.html" data-type="entity-link">ItemQueueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ItemQueueModule-d32698371d70f8d4c3dd840305dced0a"' : 'data-target="#xs-components-links-module-ItemQueueModule-d32698371d70f8d4c3dd840305dced0a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ItemQueueModule-d32698371d70f8d4c3dd840305dced0a"' :
                                            'id="xs-components-links-module-ItemQueueModule-d32698371d70f8d4c3dd840305dced0a"' }>
                                            <li class="link">
                                                <a href="components/ItemQueueComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ItemQueueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ItemSelectModule.html" data-type="entity-link">ItemSelectModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ItemSelectModule-2167b0647800bff2cab6cea4f4dde04c"' : 'data-target="#xs-components-links-module-ItemSelectModule-2167b0647800bff2cab6cea4f4dde04c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ItemSelectModule-2167b0647800bff2cab6cea4f4dde04c"' :
                                            'id="xs-components-links-module-ItemSelectModule-2167b0647800bff2cab6cea4f4dde04c"' }>
                                            <li class="link">
                                                <a href="components/ItemSelectComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ItemSelectComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ScenarioDetailEditModule.html" data-type="entity-link">ScenarioDetailEditModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ScenarioDetailEditModule-4b21e2265a025f72b87c3f7b92397cbe"' : 'data-target="#xs-components-links-module-ScenarioDetailEditModule-4b21e2265a025f72b87c3f7b92397cbe"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ScenarioDetailEditModule-4b21e2265a025f72b87c3f7b92397cbe"' :
                                            'id="xs-components-links-module-ScenarioDetailEditModule-4b21e2265a025f72b87c3f7b92397cbe"' }>
                                            <li class="link">
                                                <a href="components/ScenarioDetailEditComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScenarioDetailEditComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ScenariosListModule.html" data-type="entity-link">ScenariosListModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ScenariosListModule-f02685462592e561d67b63a136138208"' : 'data-target="#xs-components-links-module-ScenariosListModule-f02685462592e561d67b63a136138208"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ScenariosListModule-f02685462592e561d67b63a136138208"' :
                                            'id="xs-components-links-module-ScenariosListModule-f02685462592e561d67b63a136138208"' }>
                                            <li class="link">
                                                <a href="components/ScenariosListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScenariosListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ScenariosModule.html" data-type="entity-link">ScenariosModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ScenariosModule-16e7dabfa1539c941a1dd5b6b8340875"' : 'data-target="#xs-components-links-module-ScenariosModule-16e7dabfa1539c941a1dd5b6b8340875"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ScenariosModule-16e7dabfa1539c941a1dd5b6b8340875"' :
                                            'id="xs-components-links-module-ScenariosModule-16e7dabfa1539c941a1dd5b6b8340875"' }>
                                            <li class="link">
                                                <a href="components/ScenarioAssignPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScenarioAssignPageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ScenarioDetailPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScenarioDetailPageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ScenariosPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScenariosPageComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ScenariosRoutingModule.html" data-type="entity-link">ScenariosRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedMaterialModule.html" data-type="entity-link">SharedMaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TasksListModule.html" data-type="entity-link">TasksListModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TasksListModule-fa083f98ea5aabe8e34c3f2611700702"' : 'data-target="#xs-components-links-module-TasksListModule-fa083f98ea5aabe8e34c3f2611700702"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TasksListModule-fa083f98ea5aabe8e34c3f2611700702"' :
                                            'id="xs-components-links-module-TasksListModule-fa083f98ea5aabe8e34c3f2611700702"' }>
                                            <li class="link">
                                                <a href="components/TasksListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TasksListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TasksModule.html" data-type="entity-link">TasksModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ToolbarModule.html" data-type="entity-link">ToolbarModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ToolbarModule-530e252d36e776e34aa9c4be38b4330f"' : 'data-target="#xs-components-links-module-ToolbarModule-530e252d36e776e34aa9c4be38b4330f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ToolbarModule-530e252d36e776e34aa9c4be38b4330f"' :
                                            'id="xs-components-links-module-ToolbarModule-530e252d36e776e34aa9c4be38b4330f"' }>
                                            <li class="link">
                                                <a href="components/ToolbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ToolbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ToolbarTitleModule.html" data-type="entity-link">ToolbarTitleModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ToolbarTitleModule-2a55df32365d02edcfce6b5c85e30f48"' : 'data-target="#xs-components-links-module-ToolbarTitleModule-2a55df32365d02edcfce6b5c85e30f48"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ToolbarTitleModule-2a55df32365d02edcfce6b5c85e30f48"' :
                                            'id="xs-components-links-module-ToolbarTitleModule-2a55df32365d02edcfce6b5c85e30f48"' }>
                                            <li class="link">
                                                <a href="components/ToolbarTitleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ToolbarTitleComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UtilityModule.html" data-type="entity-link">UtilityModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-UtilityModule-fd16e4466e7fbe4406dd1a69ea1bf9a8"' : 'data-target="#xs-pipes-links-module-UtilityModule-fd16e4466e7fbe4406dd1a69ea1bf9a8"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-UtilityModule-fd16e4466e7fbe4406dd1a69ea1bf9a8"' :
                                            'id="xs-pipes-links-module-UtilityModule-fd16e4466e7fbe4406dd1a69ea1bf9a8"' }>
                                            <li class="link">
                                                <a href="pipes/AgePipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AgePipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/GenderPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GenderPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/ActivatedRouteStub.html" data-type="entity-link">ActivatedRouteStub</a>
                            </li>
                            <li class="link">
                                <a href="classes/AlreadyChangedError.html" data-type="entity-link">AlreadyChangedError</a>
                            </li>
                            <li class="link">
                                <a href="classes/Category.html" data-type="entity-link">Category</a>
                            </li>
                            <li class="link">
                                <a href="classes/CategoryItem.html" data-type="entity-link">CategoryItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/Child.html" data-type="entity-link">Child</a>
                            </li>
                            <li class="link">
                                <a href="classes/DialogErrorStrategy.html" data-type="entity-link">DialogErrorStrategy</a>
                            </li>
                            <li class="link">
                                <a href="classes/Dimensions.html" data-type="entity-link">Dimensions</a>
                            </li>
                            <li class="link">
                                <a href="classes/Eduapp.html" data-type="entity-link">Eduapp</a>
                            </li>
                            <li class="link">
                                <a href="classes/EduappItem.html" data-type="entity-link">EduappItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/EduappServiceSpy.html" data-type="entity-link">EduappServiceSpy</a>
                            </li>
                            <li class="link">
                                <a href="classes/ErrorStrategyImpl.html" data-type="entity-link">ErrorStrategyImpl</a>
                            </li>
                            <li class="link">
                                <a href="classes/Group.html" data-type="entity-link">Group</a>
                            </li>
                            <li class="link">
                                <a href="classes/GroupItem.html" data-type="entity-link">GroupItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/Image.html" data-type="entity-link">Image</a>
                            </li>
                            <li class="link">
                                <a href="classes/InvalidEduappError.html" data-type="entity-link">InvalidEduappError</a>
                            </li>
                            <li class="link">
                                <a href="classes/MatPaginatorIntlCz.html" data-type="entity-link">MatPaginatorIntlCz</a>
                            </li>
                            <li class="link">
                                <a href="classes/MyErrorStateMatcher.html" data-type="entity-link">MyErrorStateMatcher</a>
                            </li>
                            <li class="link">
                                <a href="classes/NoMessageErrorStrategy.html" data-type="entity-link">NoMessageErrorStrategy</a>
                            </li>
                            <li class="link">
                                <a href="classes/QueueItemImpl.html" data-type="entity-link">QueueItemImpl</a>
                            </li>
                            <li class="link">
                                <a href="classes/Scenario.html" data-type="entity-link">Scenario</a>
                            </li>
                            <li class="link">
                                <a href="classes/ScenarioQueue.html" data-type="entity-link">ScenarioQueue</a>
                            </li>
                            <li class="link">
                                <a href="classes/ScenarioQueueItem.html" data-type="entity-link">ScenarioQueueItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/ScenarioUpdate.html" data-type="entity-link">ScenarioUpdate</a>
                            </li>
                            <li class="link">
                                <a href="classes/SnackBarErrorStrategy.html" data-type="entity-link">SnackBarErrorStrategy</a>
                            </li>
                            <li class="link">
                                <a href="classes/Task.html" data-type="entity-link">Task</a>
                            </li>
                            <li class="link">
                                <a href="classes/TaskOrSettingQueueItem.html" data-type="entity-link">TaskOrSettingQueueItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/TaskQueueItem.html" data-type="entity-link">TaskQueueItem</a>
                            </li>
                            <li class="link">
                                <a href="classes/TaskSetting.html" data-type="entity-link">TaskSetting</a>
                            </li>
                            <li class="link">
                                <a href="classes/TaskSettingQueueItem.html" data-type="entity-link">TaskSettingQueueItem</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ApiService.html" data-type="entity-link">ApiService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CacheService.html" data-type="entity-link">CacheService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CategoryService.html" data-type="entity-link">CategoryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ChildService.html" data-type="entity-link">ChildService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EduappService.html" data-type="entity-link">EduappService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GroupService.html" data-type="entity-link">GroupService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/InMemoryDataService.html" data-type="entity-link">InMemoryDataService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ScenarioService.html" data-type="entity-link">ScenarioService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TaskService.html" data-type="entity-link">TaskService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/CachingInterceptor.html" data-type="entity-link">CachingInterceptor</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/InsertEduappIdInterceptor.html" data-type="entity-link">InsertEduappIdInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/UnsavedChangesCanDeactivateGuard.html" data-type="entity-link">UnsavedChangesCanDeactivateGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/CategoryOptions.html" data-type="entity-link">CategoryOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChildrenOptions.html" data-type="entity-link">ChildrenOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ChipItem.html" data-type="entity-link">ChipItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CollectionOptions.html" data-type="entity-link">CollectionOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ComponentCanDeactivate.html" data-type="entity-link">ComponentCanDeactivate</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ConfirmDialogData.html" data-type="entity-link">ConfirmDialogData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DataTableColumn.html" data-type="entity-link">DataTableColumn</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DataTableCustomColumn.html" data-type="entity-link">DataTableCustomColumn</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DialogData.html" data-type="entity-link">DialogData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DialogInput.html" data-type="entity-link">DialogInput</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ErrorConfig.html" data-type="entity-link">ErrorConfig</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ErrorDialogData.html" data-type="entity-link">ErrorDialogData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ErrorStrategy.html" data-type="entity-link">ErrorStrategy</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/GroupsOptions.html" data-type="entity-link">GroupsOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/InputDialogData.html" data-type="entity-link">InputDialogData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/QueueItem.html" data-type="entity-link">QueueItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RequestCacheEntry.html" data-type="entity-link">RequestCacheEntry</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ScenarioSaveItem.html" data-type="entity-link">ScenarioSaveItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ScenariosOptions.html" data-type="entity-link">ScenariosOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SelectItem.html" data-type="entity-link">SelectItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TaskOptions.html" data-type="entity-link">TaskOptions</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TaskSettingsOptions.html" data-type="entity-link">TaskSettingsOptions</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});